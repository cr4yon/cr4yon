import socketEventSubscribers from '../socket-event-subscribers';

export default function subscribeToSocketEvents(socket: any, store: any) {
  const subscribers = socketEventSubscribers.forStore(store);

  socket.on('drawing-event', subscribers.onDrawingEvent);

  socket.on('drawing-state', subscribers.onDrawingStateReceived);

  socket.on('room-state', subscribers.onRoomStateReceived);

  socket.on('disconnect', subscribers.onDisconnection);
}
