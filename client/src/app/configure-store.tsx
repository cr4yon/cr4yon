import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { default as rootReducer } from 'reducers';
import reduxLogger from 'redux-logger';
import { RootState } from 'state';

export function aDefaultValueFromRemote() {
  return { isFetching: false };
}

const composeEnhancers =
  typeof window === 'object' &&
  process.env.NODE_ENV === 'development' &&
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose;

function configureStore(initialState?: RootState) {
  const middlewares = process.env.NODE_ENV === 'development' ? [thunk, reduxLogger] : [thunk];
  // const middlewares = [thunk];
  const enhancer = composeEnhancers(
    applyMiddleware(...middlewares)
  );
  return createStore<any>(
    rootReducer,
    initialState,
    enhancer
  );
}

export default configureStore;
