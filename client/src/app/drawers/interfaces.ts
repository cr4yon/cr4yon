import { Point, Position } from 'models';

export interface DrawTextCommand {
  position: Position;
  color: string;
  lineWidth: number;
  textValue: string;
}

export interface Drawer {
  clean: Function;
  fillCanvasBackground: (color : string) => void;
  drawPoint: (point: Point) => any;
  drawText: (command: DrawTextCommand) => any;
  stopDrawing: () => any;
}
