import { Point, Position, DrawPointStrategy } from 'models';
import { Drawer, DrawTextCommand } from './interfaces';

export default class CanvasDrawer2D implements Drawer {
  canvas: any;
  context: any;
  drawPointStrategy: DrawPointStrategy;
  lastPoint: Point | null;

  constructor(canvas: any, context: any, drawPointStrategy: DrawPointStrategy) {
    this.canvas = canvas;
    this.context = context;
    this.drawPointStrategy = drawPointStrategy;
    this.lastPoint = null;
  }

  clean() {
    this.lastPoint = null;
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }

  fillCanvasBackground(fillingColor : string) {
    this.context.fillStyle = fillingColor;
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
  }

  stopDrawing() {
    this.lastPoint = null;
  }

  absolutePosition(position: Position) {
    return {
      x: position.x * this.canvas.width,
      y: position.y * this.canvas.height,
    };
  }

  drawText(cmd: DrawTextCommand) {
    const absolutePosition = this.absolutePosition(cmd.position);
    this.context.font = '16px Arial';
    this.context.fillStyle = cmd.color || 'black';
    this.context.fillText(cmd.textValue, absolutePosition.x, absolutePosition.y);
  }

  drawPoint(point: Point) {
    if (!this.lastPoint) {
      this.lastPoint = point;
    } else {
      switch (this.drawPointStrategy) {
        case DrawPointStrategy.DISCRETE:
          drawCircle(
            this.context,
            this.absolutePosition(point.position),
            5,
            point.color
          );
          return;
        case DrawPointStrategy.LINE:
          this.drawLine(
            this.absolutePosition(this.lastPoint.position),
            this.absolutePosition(point.position),
            point.color,
            point.lineWidth
          );
          this.lastPoint = point;
          return;
        default:
          throw new Error('Unknow draw point strategy' + this.drawPointStrategy);
      }
    }
  }

  drawLine(positionFrom: Position, positionTo: Position, color?: string, lineWidth?: number) {
    this.context.beginPath();
    if (color) {
      this.context.strokeStyle = color;
    }
    if (lineWidth) {
      this.context.lineWidth = lineWidth;
    }
    this.context.moveTo(positionFrom.x, positionFrom.y);
    this.context.lineTo(positionTo.x, positionTo.y);
    this.context.stroke();
    this.context.closePath();
  }
}


function drawCircle(context: any, positionCenter: Position, radius: number, color?: string) {
  if (color) {
    context.strokeStyle = color;
  }
  context.beginPath();
  context.arc(positionCenter.x, positionCenter.y, radius, 0, 2 * Math.PI);
  context.stroke();
}
