import * as React from 'react';
import {
  BrowserRouter as ReactRouter,
  Redirect,
  Route,
  Switch } from 'react-router-dom';
import configureStore from './configure-store';
import { Provider } from 'react-redux';
import Router from 'components/Router';
import HomePageMain from 'components/HomePageMain';
import io from 'socket.io-client';
import { AuthenticationStatus, DrawingActionType } from 'state';
import { DrawingEventType } from 'models';
import subscribeToStateObservables from './subscribe-to-state-observables';
import subscribeToSocketEvents from './subscribe-to-socket-events';
import FeedbackPage from 'pages/FeedbackPage';

console.log('process.env.API_URL', process.env.API_URL);
console.log('window.config', (window as any).config);
let API_URL = process.env.API_URL || '';
if (!API_URL && window && (window as any).config && (window as any).config.apiUrl) {
  console.log('API_URL not set, apiUrl exists in window config exists, will use');
  API_URL = (window as any).config.apiUrl;
}
// const socket = io.connect(API_URL, {
//   reconnection: true,
//   reconnectionDelay: 1000,
//   reconnectionDelayMax : 5000,
//   reconnectionAttempts: 10,
// });

console.log(`Will use API URL ${API_URL}`);
const socket = io.connect(API_URL);

console.log('socket created', socket);

const store = configureStore({
  socket,
  drawingHistory: [],
  drawingState: {
    lastEventType: DrawingEventType.DRAW_POINT,
    drawingActionType: DrawingActionType.POINT,
    inProgress: false,
  },
  color: '#000000',
  palette: {
    color: '#000000',
    lineWidth: 3,
  },
  authentication: { status: AuthenticationStatus.PENDING },
});

subscribeToStateObservables(store);
subscribeToSocketEvents(socket, store);

const App = () => (
  <Provider store={store}>
    <ReactRouter>
      <Switch>
        <Route
          exact={true}
          path="/"
          // tslint:disable-next-line
          render={() => (
            <HomePageMain
              onGetStartedRender={() => (<Redirect push={true} to="/whiteboard" />)}
              onGiveFeedbackRender={() => (<Redirect push={true} to="/feedback" />)}
            />
          )}
        />
        <Route path="/whiteboard" component={Router} />
        <Route
          path="/feedback"
          // tslint:disable-next-line
          render={() => (
            <FeedbackPage
              goBackRender={() => (<Redirect push={true} to="/" />)}
            />
          )}
        />
      </Switch>
    </ReactRouter>
  </Provider>
);

export default App;
