import * as React from 'react';
import { Action, CHOOSE_LINE_WIDTH } from 'actions';
import { connect } from 'react-redux';
import { RootState } from 'app/state';

interface PublicProps {}

const mapStateToProps = (state: RootState, ownProps: PublicProps) : Partial<Props> => {
  return {
    ...ownProps,
    currentLineWidth: state.palette.lineWidth,
    sampleColor: state.palette.color,
  };
};

const mapDispatchToProps = (dispatch: (action: Action) => any,
                            ownProps: PublicProps) : Partial<Props> => {
  return {
    ...ownProps,
    chooseLineWidth: (lineWidth: number) => dispatch(CHOOSE_LINE_WIDTH.creator(lineWidth)),
  };
};

interface Props {
  chooseLineWidth: (lineWidth: number) => any;
  currentLineWidth: number;
  sampleColor: string;
}

interface State {
  inputValue: number;
}

class LineWidthPicker extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { inputValue: props.currentLineWidth };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event: any) {
    this.setState({ inputValue: event.target.value });
    this.props.chooseLineWidth(event.target.value);
  }

  handleSubmit(event: any) {
    event.preventDefault();
  }

  isInputValueValid() {
    return !!this.state.inputValue;
  }

  render() {
    const sampleStyles = {
      height: `${this.state.inputValue}px`,
      width: `${this.state.inputValue}px`,
      backgroundColor: this.props.sampleColor,
      display: 'inline-block',
    };
    return (
      <form className="form-inline" onSubmit={this.handleSubmit}>
        <div
          className="form-row align-items-center"
          style={{ width: '100%' }}
        >
          <div className="col-10">
            <div
              className="input-group"
              style={{ width: '100%' }}
            >
              <input
                style={{ width: '100%' }}
                id="chooseLineWidthInput"
                type="range"
                min="1"
                max="50"
                step="1"
                value={this.state.inputValue}
                onChange={this.handleChange}
                list="widthOptions"
              />
              <datalist id="widthOptions">
                <option value="1" label="1px"/>
                <option value="2"/>
                <option value="3" label="3px"/>
                <option value="5"/>
                <option value="8" label="8px"/>
                <option value="13"/>
                <option value="21" label="21px"/>
                <option value="33"/>
                <option value="54" label="54px"/>
                <option value="87"/>
                {/* <option value="100"/> */}
              </datalist>
            </div>
          </div>
          <div className="col-2">
            <div className="input-group-text">
              <div style={sampleStyles} className="mx-auto"/>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

export { LineWidthPicker };
export default connect(mapStateToProps, mapDispatchToProps)(LineWidthPicker);
