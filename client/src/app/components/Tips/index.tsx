import * as React from 'react';

class Tips extends React.PureComponent<any> {
  constructor(props: any) {
    super(props);
  }

  render() {
    return (
      <div>
        <ul>
          <li>
            Double click to draw text
          </li>
          <li>
            Select color white and increase line width to erase
          </li>
        </ul>
      </div>
    );
  }
}

export default Tips;
