import * as React from 'react';
import GoogleFeedbackForm from './GoogleFeedbackForm';

const GOOGLE_FORM_URL = `https://docs.google.com/forms/d/e/
1FAIpQLSdAXB1iAwBosxvgxD4zqeuSnZUQNaB9X0FmzCKWeaON4gryew/viewform?embedded=true`;

interface Props {
  width: number;
  height: number;
}

export default class FeedbackForm extends React.PureComponent<Props> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <div>
        <GoogleFeedbackForm
          embeddedFormUrl={GOOGLE_FORM_URL}
          width={this.props.width}
          height={this.props.height}
        />
      </div>
    );
  }
}
