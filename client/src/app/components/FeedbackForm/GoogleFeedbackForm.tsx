import * as React from 'react';

interface Props {
  embeddedFormUrl: string;
  width: number;
  height: number;
}

export default class GoogleFeedbackForm extends React.PureComponent<Props> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <div>
        <iframe
          style={{ width: '100%' }}
          src={this.props.embeddedFormUrl}
          width={`${this.props.width}`}
          height={`${this.props.height}`}
          frameBorder="0"
          marginHeight={0}
          marginWidth={0}
        >
          Loading feedback form...
        </iframe>
      </div>
    );
  }
}
