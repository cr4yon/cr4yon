import * as React from 'react';
import CrayonImage from '../CrayonImage';

interface Props {
  maxHeight: string;
}

class LogoWithText extends React.PureComponent<Props> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <div className="my-2">
        <div className="float-left px-1">
          <CrayonImage maxHeight={this.props.maxHeight} />
        </div>
        <div className="small float-left px-1">
          <span className="badge badge-info badge-sm">&#945;</span>
        </div>
        <h2> Crayon </h2>
      </div>
    );
  }
}

export default LogoWithText;
