import * as React from 'react';
import decode from 'urldecode';

import { Action, JOIN_WHITEBOARD } from 'actions';
import { connect } from 'react-redux';
import { RootState } from 'app/state';

interface PublicProps {}

const mapStateToProps = (state: RootState, ownProps: PublicProps) : Partial<Props> => {
  return {
    ...ownProps,
  };
};

const mapDispatchToProps = (dispatch: (action: Action | Function) => any,
                            ownProps: PublicProps) : Partial<Props> => {
  return {
    ...ownProps,
    joinWhiteBoard: (userName: string, roomName: string) => {
      dispatch(JOIN_WHITEBOARD.creator(userName, roomName));
    },
  };
};

interface Props {
  joinWhiteBoard: (userName: string, roomName: string) => any;
}

interface State {
  userNameInputValue: string;
  roomNameInputValue: string;
}

function getURLParameter(sParam: string) {
  const pageURL = window.location.search.substring(1);
  const urlVariables = pageURL.split('&');
  const parameter = urlVariables.find(v => v.split('=')[0] === sParam);

  return parameter ? parameter.split('=')[1] : null;
}

class EntryForm extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { userNameInputValue: '', roomNameInputValue: '' };

    this.handleUserNameChange = this.handleUserNameChange.bind(this);
    this.handlRoomNameChange = this.handlRoomNameChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillMount() {
    const roomNameFromUrlParams = getURLParameter('room');
    if (roomNameFromUrlParams && this.isNonEmptyString(roomNameFromUrlParams)) {
      this.setState({ roomNameInputValue: decode(roomNameFromUrlParams) });
    }
  }

  handleUserNameChange(event: any) {
    this.setState({ userNameInputValue: event.target.value });
  }

  handlRoomNameChange(event: any) {
    this.setState({ roomNameInputValue: event.target.value });
  }

  handleSubmit(event: any) {
    console.log('Trying to join room');
    this.props.joinWhiteBoard(this.state.userNameInputValue, this.state.roomNameInputValue);
    event.preventDefault();
  }

  isFormValid() {
    return this.isNonEmptyString(this.state.userNameInputValue) &&
     this.isNonEmptyString(this.state.roomNameInputValue);
  }

  isNonEmptyString(str: string | null | undefined) {
    return str && str !== '';
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>

        <div className="form-group">
          <label htmlFor="chooseUserNameInput">
            Your name
          </label>
          <div className="input-group">
            <input
              id="chooseUserNameInput"
              type="text"
              className="form-control input-sm"
              value={this.state.userNameInputValue}
              onChange={this.handleUserNameChange}
            />
          </div>
        </div>

        <div className="form-group">
          <label htmlFor="chooseRoomNameInput">
            Room to join
          </label>
          <div className="input-group">
            <input
              id="chooseRoomNameInput"
              type="text"
              className="form-control input-sm"
              value={this.state.roomNameInputValue}
              onChange={this.handlRoomNameChange}
            />
          </div>
        </div>

        <div className="row justify-content-end">
          <div className="col">
            <button
              type="submit"
              className="btn btn-primary input-sm"
              disabled={!this.isFormValid()}
            >
              Join
            </button>
          </div>
        </div>
      </form>
    );
  }
}

export { EntryForm };
export default connect(mapStateToProps, mapDispatchToProps)(EntryForm);
