import * as React from 'react';
import { Action } from 'actions';
import { connect } from 'react-redux';
import { Authentication, Room, RootState } from 'app/state';
import EntryForm from 'components/EntryForm';
import GlobalError from 'components/GlobalError';
import FeedbackPage from 'pages/FeedbackPage';
import WhiteBoardPage from 'pages/WhiteBoardPage';
import LogoWithText from 'components/LogoWithText';

interface PublicProps {}

const mapStateToProps = (state: RootState, ownProps: PublicProps) : Partial<Props> => {
  return {
    ...ownProps,
    authentication: state.authentication,
    room: state.room,
  };
};

const mapDispatchToProps = (dispatch: (action: Action) => any,
                            ownProps: PublicProps) : Partial<Props> => {
  return {
    ...ownProps,
  };
};

interface Props {
  authentication: Authentication;
  room: Room;
}

enum PAGE {
  ENTRY_FORM = 'ENTRY_FORM',
  WHITE_BOARD = 'WHITE_BOARD',
  FEEDBACK_FORM = 'FEEDBACK_FORM',
}

interface State {
  page: PAGE;
}

class Router extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = this.decideState(props);

    this.goToFeedbackPage = this.goToFeedbackPage.bind(this);
    this.goToWhiteBoardPage = this.goToWhiteBoardPage.bind(this);
  }

  decideState(props: Props) {
    if (this.isUserAuthenticated() && this.isRoomJoined()) {
      return {
        page: PAGE.WHITE_BOARD,
      };
    } else {
      return {
        page: PAGE.ENTRY_FORM,
      };
    }
  }

  componentWillReceiveProps(props: Props) {
    this.setState(this.decideState(props));
  }

  goToPage(page: PAGE) {
    this.setState({
      page,
    });
  }

  goToFeedbackPage() {
    this.goToPage(PAGE.FEEDBACK_FORM);
  }

  goToWhiteBoardPage() {
    this.goToPage(PAGE.WHITE_BOARD);
  }

  getPageToRender() {
    switch (this.state.page) {
      case PAGE.WHITE_BOARD:
        return (
          <WhiteBoardPage
            goToFeedbackPage={this.goToFeedbackPage}
          />
        );
      case PAGE.ENTRY_FORM:
        return (
          <div className="container-fluid">
            <LogoWithText maxHeight={'30px'}/>
            <div className="row justify-content-center">
              <div className="col-sm-6 col-md-4">
                <EntryForm/>
              </div>
            </div>
          </div>
        );
      case PAGE.FEEDBACK_FORM:
        return (
          <FeedbackPage
            goBack={this.goToWhiteBoardPage}
          />
        );
    }
  }

  isUserAuthenticated() {
    return this.props.authentication && this.props.authentication.userName;
  }

  isRoomJoined() {
    return this.props.room && this.props.room.name;
  }

  render() {
    const styles = {};
    const classes = '';
    if (this.state.page === PAGE.WHITE_BOARD) {
      styles['backgroundColor'] = '#555';
    }

    return (
      <div className={classes} style={styles}>
        <GlobalError/>
        {
          this.getPageToRender()
        }
      </div>
    );
  }
}

export { Router };
export default connect(mapStateToProps, mapDispatchToProps)(Router);
