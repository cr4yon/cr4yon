import * as React from 'react';


interface SquarePickProps {
  onClick: (color: string) => void;
  color: string;
  highlighted?: boolean;
}

class SquarePick extends React.PureComponent<SquarePickProps> {
  constructor(props: SquarePickProps) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick(e: any) {
    e.preventDefault();
    this.props.onClick(this.props.color);
  }

  render() {
    const styles = {
      backgroundColor: this.props.color,
      height: '24px',
      width: '100%',
      minWidth: '20px',
      display: 'inline-block',
      border: '1px solid #eee',
    };
    if (this.props.highlighted) {
      styles['boxShadow'] = `0px 2px 3px ${this.props.color}`;
    }
    return (
      <div style={styles} onClick={this.onClick}/>
    );
  }
}

interface Props {
  onChangeComplete: (color: {hex: string}) => any;
  color: string;
}

interface State {
  selected: string | null;
}

const color = (hex: string, name: string) => ({ hex, name });
const DEFAULT_COLORS = [
  color('#FF6900', 'orange'),
  color('#FCB900', 'yellow'),
  color('#0693E3', 'blue'),
  color('#EB144C', 'red'),
  color('#9900EF', 'purple'),
  color('#FFFFFF', 'white'),
  color('#000000', 'black'),
  color('#00D384', 'green'),
];


class ColorPicker extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { selected: null };

    this.handleChangeComplete = this.handleChangeComplete.bind(this);
  }

  handleChangeComplete(color: any) {
    this.setState({ selected: color });
    this.props.onChangeComplete({
      hex: color,
    });
  }

  render() {
    return (
      <div className="row py-2">
        { DEFAULT_COLORS.map((c: any, i: number) => (
          <div className="col p-0" key={i}>
            <SquarePick
              onClick={this.handleChangeComplete}
              color={c.hex}
              highlighted={c.hex === this.state.selected}
            />
          </div>
        ))}
      </div>
    );
  }
}

export default ColorPicker;
