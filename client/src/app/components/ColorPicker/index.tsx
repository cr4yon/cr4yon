import * as React from 'react';
import { Action, CHOOSE_COLOR } from 'actions';
import { connect } from 'react-redux';
import { RootState } from 'app/state';
// import { TwitterPicker as ExternalPicker } from 'react-color';
import { default as InternalPicker } from './InternalPicker';

interface PublicProps {}

const mapStateToProps = (state: RootState, ownProps: PublicProps) : Partial<Props> => {
  return {
    ...ownProps,
    currentColor: state.color,
  };
};

const mapDispatchToProps = (dispatch: (action: Action) => any,
                            ownProps: PublicProps) : Partial<Props> => {
  return {
    ...ownProps,
    chooseColor: (color: string) => dispatch(CHOOSE_COLOR.creator(color)),
  };
};

interface Props {
  chooseColor: (color: string) => any;
  currentColor: string;
}

interface State {
  inputValue: string;
}

class ColorPicker extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { inputValue: '' };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeComplete = this.handleChangeComplete.bind(this);
  }

  handleChange(event: any) {
    this.setState({ inputValue: event.target.value });
  }

  handleSubmit(event: any) {
    this.props.chooseColor(this.state.inputValue);
    event.preventDefault();
  }

  handleChangeComplete(color: any) {
    this.props.chooseColor(color.hex);
  }

  isInputValueValid() {
    return this.state.inputValue.length === 7;
  }

  render() {
    return (
      <InternalPicker
        color={this.props.currentColor}
        onChangeComplete={this.handleChangeComplete}
      />
    );
  }
}

export { ColorPicker };
export default connect(mapStateToProps, mapDispatchToProps)(ColorPicker);
