import * as React from 'react';
import { Action } from 'actions';
import { connect } from 'react-redux';
import { RootState } from 'app/state';
import { Room } from 'state';

interface PublicProps {}

const mapStateToProps = (state: RootState, ownProps: PublicProps) : Partial<Props> => {
  return {
    ...ownProps,
    userName: state.authentication.userName,
    roomName: state.room && state.room.name,
    room: state.room,
  };
};

const mapDispatchToProps = (dispatch: (action: Action) => any,
                            ownProps: PublicProps) : Partial<Props> => {
  return {
    ...ownProps,
  };
};

interface Props {
  userName: string;
  roomName: string | undefined;
  room: Room;
}

interface MemberProps {
  userName: string;
  isConnected: boolean;
  isDrawing: boolean;
  drawingColor?: string;
}

class Member extends React.PureComponent<MemberProps> {
  constructor(props: MemberProps) {
    super(props);
  }

  render() {
    return this.props.isConnected ?
      (
        <strong style={{ color: this.props.drawingColor || '#000' }}>
          {this.props.userName}{this.props.isDrawing && ` (...)`}
        </strong>
      )
      :
      `${this.props.userName}`
    ;
  }
}

class RoomInfo extends React.PureComponent<Props> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    const members = this.props.room.members;
    return (
      <div className="row">
        <div
          className="col-12 col-sm-4"
          style={{}}
        >
          <h4>🖍 {this.props.roomName}</h4>
        </div>
        <div className="col-12 col-sm-8" style={{ backgroundColor: '#e6e6e6' }}>
          <p>
            {members.map((m, i) => (
                <span key={i}>
                  {i > 0 && ', '}
                  <Member
                    userName={m.userName}
                    isConnected={m.isConnected}
                    isDrawing={m.isDrawing}
                    drawingColor={m.drawingColor}
                  />
                </span>
              ))
            } {members.length > 1 ? 'are' : 'is'} here.
          </p>
        </div>
      </div>
    );
  }
}

export { RoomInfo };
export default connect(mapStateToProps, mapDispatchToProps)(RoomInfo);
