import * as React from 'react';
import { Action } from 'actions';
import { connect } from 'react-redux';
import { RootState } from 'app/state';

interface PublicProps {}

const mapStateToProps = (state: RootState, ownProps: PublicProps) : Partial<Props> => {
  return {
    ...ownProps,
    error: state.error,
  };
};

const mapDispatchToProps = (dispatch: (action: Action) => any,
                            ownProps: PublicProps) : Partial<Props> => {
  return {
    ...ownProps,
  };
};

interface Props {
  error: string;
}

class GlobalError extends React.PureComponent<Props> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    return this.props.error ? (
        <div className="alert alert-danger">
          <p> {this.props.error} </p>
        </div>
      ) : null;
  }
}

export { GlobalError };
export default connect(mapStateToProps, mapDispatchToProps)(GlobalError);
