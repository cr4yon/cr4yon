import * as React from 'react';
import image from './example.png';
import LogoWithText from 'components/LogoWithText';

interface Props {
  onGetStartedRender: Function;
  onGiveFeedbackRender: Function;
  maxHeight?: string;
}

interface State {
  getStartedClicked: boolean;
  giveFeedbackClicked: boolean;
}

export default class HomePageMain extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      getStartedClicked: false,
      giveFeedbackClicked: false,
    };

    this.onGetStartedClick = this.onGetStartedClick.bind(this);
    this.onGiveFeedbackClick = this.onGiveFeedbackClick.bind(this);
  }

  onGetStartedClick(e: any) {
    e.preventDefault();
    this.setState({ getStartedClicked: true });
  }

  onGiveFeedbackClick(e: any) {
    e.preventDefault();
    this.setState({ giveFeedbackClicked: true });
  }

  render() {
    if (this.state.getStartedClicked) {
      return this.props.onGetStartedRender();
    } else if (this.state.giveFeedbackClicked) {
      return this.props.onGiveFeedbackRender();
    } else {
      return (
        <div>
          <header
            className="sticky-top"
            style={{
              width: '100%',
              boxShadow: '0px 0px 3px #ccc',
              backgroundColor: '#fff',
            }}
          >
            <div className="container">
              <div className="row">
                <div className="col-6 col-sm-4">
                  <LogoWithText maxHeight={'30px'}/>
                </div>
              </div>
            </div>
          </header>

          <main>
            <div
              className="jumbotron jumbotron-fluid"
              style={{
                backgroundColor: '#fff',
              }}
            >
              <div className="container">
                <div className="row">
                  <div className="col-12 col-md-6">
                    <h1>
                      Where minds come together
                    </h1>
                    <p>
                      When you need a decision to emerge naturally from white-boarding.
                    </p>
                    <div>
                      <button className="btn btn-primary" onClick={this.onGetStartedClick}>
                        Get started
                      </button>
                    </div>
                  </div>
                  <div className="col-12 col-md-6 order-md-first">
                    <img
                      src={image}
                      style={{
                        maxHeight: this.props.maxHeight || '400px',
                        maxWidth: '100%',
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>

            <div
              className="jumbotron jumbotron-fluid"
              style={{
                backgroundColor: 'rgb(56, 255, 176)',
              }}
            >
              <div className="container">
                <h2>
                  We're in alpha
                </h2>
                <p>
                  Lucky you! You get to try out our collaborative white board and give feedback
                </p>
                <div>
                  <button className="btn btn-primary" onClick={this.onGiveFeedbackClick}>
                    Give feedback 🙌
                  </button>
                </div>
              </div>
            </div>
          </main>
        </div>
      );
    }
  }
}
