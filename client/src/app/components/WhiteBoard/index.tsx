import * as React from 'react';
import { Drawer, CanvasDrawer2D } from 'drawers';

// should separate models per feature: drawer, userIntentListener, etc...
import {
  DrawPointStrategy,
  Point,
  DrawingEvent,
  DrawingEventType,
  StartDrawingTextEvent,
  Position,
} from 'models';
import {
  Action,
  HANDLE_LOCAL_DRAWING_EVENT,
  START_DRAWING_TEXT,
} from 'actions';
import {
  UserIntentListener,
  CanvasUserIntentMouseListener,
  CanvasUserIntentTouchListener,
} from 'user-intent-listeners';
import { connect } from 'react-redux';
import { RootState, Palette, DrawingActionType, DrawingHistoricalEvent } from 'state';

interface PublicProps {
  contextType: string;
  width: number;
  height: number;
}

const mapStateToProps = (state: RootState, ownProps: PublicProps) : Partial<Props> => {
  return {
    ...ownProps,
    isDrawing: state.drawingState.drawingActionType === DrawingActionType.POINT
      && state.drawingState.inProgress,
    drawingHistory: state.drawingHistory,
    color: state.color,
    palette: state.palette,
    whiteBoardFillingColor: '#ffffff',
  };
};

const mapDispatchToProps = (dispatch: (action: Action | Function) => any,
                            ownProps: PublicProps) : Partial<Props> => {
  return {
    ...ownProps,
    drawPoint: (point: Point) => {
      dispatch(
        HANDLE_LOCAL_DRAWING_EVENT.creator(DrawingEventType.DRAW_POINT, point, {})
      );
    },
    stopDrawing: (point: Point) => {
      dispatch(
        HANDLE_LOCAL_DRAWING_EVENT.creator(DrawingEventType.STOP_DRAWING, point, {})
      );
    },
    startDrawingText: (point: Point, pagePosition: Position) => {
      dispatch(START_DRAWING_TEXT.creator(point, pagePosition));
      // TODO: use normal drawing event
    },
  };
};

interface Props {
  isDrawing: boolean;
  contextType: string;
  width: number;
  height: number;
  drawingHistory: DrawingHistoricalEvent[];
  color: string;
  whiteBoardFillingColor?: string;
  palette: Palette;
  drawPoint: (point: Point) => any;
  stopDrawing: (point: Point) => any;
  startDrawingText: (point: Point, pagePosition: Position) => any;
}

class WhiteBoard extends React.Component<Props, any> {
  canvas: any;
  context: any;
  drawer: Drawer;
  otherDrawers: Map<String, Drawer>;
  userIntentListeners: UserIntentListener[];

  constructor(props: Props) {
    super(props);
  }

  componentDidMount() {
    this.context = this.getContextOrError();

    this.drawer = this.getDrawer();
    this.otherDrawers = new Map();

    if (this.props.whiteBoardFillingColor) {
      this.fillCanvasBackground();
    }
    this.drawHistory(this.props.drawingHistory);

    this.userIntentListeners = [
      new CanvasUserIntentTouchListener(
        this.canvas,
        this.getDrawingEventHandler()
      ),
      new CanvasUserIntentMouseListener(
        this.canvas,
        this.getDrawingEventHandler()
      ),
    ];

    this.userIntentListeners.forEach(listener => listener.start());
  }

  fillCanvasBackground() {
    this.drawer.fillCanvasBackground(this.props.whiteBoardFillingColor || '#fff');
  }

  eraseAll() {
    this.drawer.clean();
  }

  getDrawer() {
    return new CanvasDrawer2D(this.canvas, this.getContextOrError(), DrawPointStrategy.LINE);
  }

  getDrawingEventHandler() {
    return (drawingEvent: DrawingEvent) => {
      switch (drawingEvent.type) {
        case DrawingEventType.DRAW_POINT:
          this.props.drawPoint(
            {
              ...drawingEvent.point,
              color: this.props.color,
              lineWidth: this.props.palette.lineWidth,
            }
          );
          return;
        case DrawingEventType.STOP_DRAWING:
          this.props.stopDrawing(drawingEvent.point);
          return;
        case DrawingEventType.START_DRAWING_TEXT:
          this.props.startDrawingText(
            drawingEvent.point,
            (drawingEvent as StartDrawingTextEvent).absolutePagePosition
          );
          return;
        default:
          throw new Error(`Cannot handle drawing event type ${drawingEvent.type}`);
      }
    };
  }

  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.isDrawing !== this.props.isDrawing) {
      if (!nextProps.isDrawing) {
        this.stopDrawing();
      }
    }

    if (nextProps.drawingHistory !== this.props.drawingHistory) {
      this.drawNewHistory(nextProps);
    }
  }

  stopDrawing() {
    this.drawer.stopDrawing();
  }

  redrawAllPoints() {
    this.eraseAll();
    this.fillCanvasBackground();
    this.drawHistory(this.props.drawingHistory);
  }

  drawNewHistory(nextProps: Props) {
    if (nextProps.drawingHistory.length > this.props.drawingHistory.length) {
      this.drawHistory(nextProps.drawingHistory.slice(this.props.drawingHistory.length));
    } else {
      this.eraseAll();
      this.fillCanvasBackground();
      this.drawHistory(nextProps.drawingHistory);
    }
  }

  drawHistory(historicalEvents: DrawingHistoricalEvent[]) {
    historicalEvents.forEach((event) => {
      const drawTextCommand = {
        position: event.point.position,
        color: event.point.color || '#000000', // fix
        lineWidth: event.point.lineWidth || 2, // fix
        textValue: event.details.textValue,
      };
      let drawer;
      if (event.point.user) {
        const userName = event.point.user;
        if (!this.otherDrawers.has(userName)) {
          this.otherDrawers.set(userName, this.getDrawer());
        }
        drawer = this.otherDrawers.get(userName);
      } else {
        drawer = this.drawer;
      }
      switch (event.type) {
        case DrawingEventType.DRAW_TEXT:
          drawer!.drawText(drawTextCommand);
          return;
        case DrawingEventType.STOP_DRAWING:
          drawer!.stopDrawing();
          return;
        case DrawingEventType.DRAW_POINT:
          drawer!.drawPoint(event.point);
          return;
        default:
          console.error('No handling of historical event', event.type);
      }
    });
  }

  shouldComponentUpdate() {
    return false;
  }

  getContextOrError() {
    const context = this.canvas.getContext(this.props.contextType);
    if (!context) {
      const err = `No ${this.props.contextType} context`;
      alert(err);
      throw new Error(err);
    } else {
      return context;
    }
  }

  render() {
    const styles = {
      width: '100%',
      height: 'auto',
      border: 'solid grey 1px',
      cursor: 'pointer',
    };
    return (
      <div>
        <canvas
          id="whiteboard"
          width={this.props.width}
          height={this.props.height}
          style={styles}
          ref={(input) => { this.canvas = input; }}
        />
      </div>
    );
  }
}

export { WhiteBoard };

export default connect(mapStateToProps, mapDispatchToProps)(WhiteBoard);
