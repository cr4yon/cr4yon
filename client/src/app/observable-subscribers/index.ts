import { RootState } from 'state';

export function roomStateLoadedSubscriber(store: any) {
  return (state: RootState) => {
    const roomName = state.room!.name;
    const params = new URLSearchParams(location.search);
    params.set('room', roomName);

    window.history.replaceState({}, '', `${location.pathname}?${params}`);
  };
}
