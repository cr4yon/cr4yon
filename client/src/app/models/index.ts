export enum PointType {
  START = 'start',
  STOP = 'stop',
}

export interface Point {
  position: Position;
  type?: PointType;
  color?: string;
  lineWidth?: number;
  user?: string;
}

export interface Position {
  x: number;
  y: number;
}

export enum DrawPointStrategy {
  LINE = 'line',
  DISCRETE = 'discrete',
}

export enum DrawingEventType {
  DRAW_POINT = 'event.drawPoint',
  STOP_DRAWING = 'event.stopDrawing',
  START_DRAWING_TEXT = 'event.startDrawingText',
  DRAW_TEXT = 'event.drawText',
  UNDO = 'event.undo',
}

export class DrawingEvent {
  point: Point;
  type: DrawingEventType;

  constructor(type: DrawingEventType, point: Point) {
    this.type = type;
    this.point = point;
  }
}

export class StartDrawingTextEvent extends DrawingEvent {
  absolutePagePosition: Position;
  constructor(point: Point, absolutePagePosition: Position) {
    super(DrawingEventType.START_DRAWING_TEXT, point);
    this.absolutePagePosition = absolutePagePosition;
  }
}

export class DrawPointEvent extends DrawingEvent {
  constructor(point: Point) {
    super(DrawingEventType.DRAW_POINT, point);
  }
}

export class StopDrawingEvent extends DrawingEvent {
  constructor(point: Point) {
    super(DrawingEventType.STOP_DRAWING, point);
  }
}
