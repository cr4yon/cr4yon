import * as React from 'react';
import { connect } from 'react-redux';
import ColorPicker from 'components/ColorPicker';
import LineWidthPicker from 'components/LineWidthPicker';
import WhiteBoard from 'components/WhiteBoard';
import RoomInfo from 'components/RoomInfo';
import DrawTextForm from 'components/DrawTextForm';
import Tips from 'components/Tips';
import UndoRedo from 'components/UndoRedo';
import { RootState } from 'state';
import {
  Action,
  HANDLE_LOCAL_DRAWING_EVENT,
} from 'actions';
import { Point, Position, DrawingEventType } from 'models';

interface PublicProps {}

const isDrawingText = (state: RootState) => (
  state.drawingState.lastEventType === DrawingEventType.DRAW_TEXT && state.drawingState.inProgress
);

const mapStateToProps = (state: RootState, ownProps: PublicProps) : Partial<Props> => {
  return {
    ...ownProps,
    showDrawingTextForm: isDrawingText(state),
    textToDrawPoint: isDrawingText(state) && state.drawingState.details.point,
    pagePositionOfTextToDraw: isDrawingText(state) && state.drawingState.details.pagePosition,
  };
};

const mapDispatchToProps = (dispatch: (action: Action | Function) => any,
                            ownProps: PublicProps) : Partial<Props> => {
  return {
    ...ownProps,
    onTextToDrawSubmit: (point: Point, text: string) => (
      dispatch(
        HANDLE_LOCAL_DRAWING_EVENT.creator(DrawingEventType.DRAW_TEXT, point, { textValue: text })
      )
    ),
    onUndo: () => (
      dispatch(
        HANDLE_LOCAL_DRAWING_EVENT.creator(
          DrawingEventType.UNDO,
          { position: { x: 0, y: 0 } }, // hack
          {}
        )
      )
    ),
  };
};

interface Props {
  showDrawingTextForm: boolean;
  textToDrawPoint?: Point;
  pagePositionOfTextToDraw?: Position;
  onTextToDrawSubmit: (point: Point, text: string) => void;
  onUndo: () => void;
  goToFeedbackPage: Function;
}

interface State {
  showTips: boolean;
}

class WhiteBoardPage extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      showTips: false,
    };

    this.onGoToFeedbackPageClick = this.onGoToFeedbackPageClick.bind(this);
    this.onTextToDrawValueSubmit = this.onTextToDrawValueSubmit.bind(this);
    this.toggleTips = this.toggleTips.bind(this);
  }

  onGoToFeedbackPageClick(event: any) {
    event.preventDefault();
    this.props.goToFeedbackPage();
  }

  toggleTips(event: any) {
    event.preventDefault();
    this.setState({ showTips: !this.state.showTips });
  }

  onTextToDrawValueSubmit(text: string) {
    this.props.onTextToDrawSubmit(
      this.props.textToDrawPoint!,
      text
    );
  }

  render() {
    return (
      <div>
        <header
          className="sticky-top"
          style={{
            width: '100%',
            boxShadow: '0px 0px 3px #ccc',
            backgroundColor: '#fff',
          }}
        >
          <div className="container-fluid">
            <div className="row">
              <div className="col-12 col-sm-9">
               <RoomInfo/>
              </div>
              <div className="d-sm-inline-block col-sm-3 text-right">
                <button
                  onClick={this.onGoToFeedbackPageClick}
                  className="btn btn-link"
                >
                  Feedback 🙌
                </button>
              </div>
            </div>
          </div>
        </header>
        <main
          className="container-fluid p-1 p-sm-2"
          style={{
            marginBottom: '40px',
            maxWidth: '1000px',
          }}
        >
          <div className="text-center">
            <WhiteBoard
              contextType={'2d'}
              width={800}
              height={500}
            />
          </div>
          { this.props.showDrawingTextForm &&
            <div
              style={{
                position: 'fixed',
                left: `${this.props.pagePositionOfTextToDraw!.x}px`,
                top: `${this.props.pagePositionOfTextToDraw!.y}px`,
                zIndex: 1000,
              }}
            >
              <DrawTextForm
                onSubmit={this.onTextToDrawValueSubmit}
              />
            </div>
          }
        </main>
        <footer
          style={{
            position: 'fixed',
            bottom: '0',
            left: '0',
            width: '100%',
            boxShadow: '0px 0px 3px #ccc',
            backgroundColor: '#fff',
          }}
        >
          <div className="container">
            { this.state.showTips &&
              <div className="row py-1 justify-content-center align-items-center text-info">
                <div className="col-12 col-sm-6">
                  <Tips/>
                </div>
              </div>
            }
            <div className="row py-1 justify-content-center align-items-center">
              <div className="col-6 col-sm-2 text-center">
                <button
                  onClick={this.toggleTips}
                  className="btn btn-link"
                >
                  Tips
                </button>
              </div>
              <div className="col-6 col-sm-2">
                <UndoRedo onUndo={this.props.onUndo}/>
              </div>
              <div className="col-12 col-sm-4">
                <LineWidthPicker/>
              </div>
              <div className="col-12 col-sm-4">
                <ColorPicker/>
              </div>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}

export { WhiteBoardPage };
export default connect(mapStateToProps, mapDispatchToProps)(WhiteBoardPage);
