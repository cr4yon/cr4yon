import {
  DrawPointEvent,
  StopDrawingEvent,
  DrawingEvent,
  StartDrawingTextEvent,
} from 'models';
import SimpleEventPublisher from './SimpleEventPublisher';

interface MouseEvent {
  x: number;
  y: number;
  pageX: number;
  pageY: number;
  preventDefault: Function;
}

const DOUBLE_CLICK_MAX_INTERVAL = 500;

export default class CanvasUserIntentMouseListener {
  canvas: any;
  drawingEventHandler: (drawingEvent: DrawingEvent) => any;
  lastTapTime: number;
  internalEventPublisher: SimpleEventPublisher;
  mouseIsDown: boolean;

  constructor(canvas: any, drawingEventHandler: (drawingEvent: DrawingEvent) => any) {
    this.canvas = canvas;
    this.drawingEventHandler = drawingEventHandler;
    this.internalEventPublisher = new SimpleEventPublisher();
    this.mouseIsDown = false;
  }

  start() {
    this.setUpEventListeners();
    this.setUpInternalEventSubscribers();
  }

  setUpEventListeners() {
    this.canvas.addEventListener('mousedown', (event: any) => {
      this.onMouseDownHandler(event);
    });
    this.canvas.addEventListener('mouseup', (event: MouseEvent) => {
      this.onMouseUpHandler(event);
    });
  }

  setUpInternalEventSubscribers() {
    this.internalEventPublisher.addSubscriber('doubleClick', (event: MouseEvent) => {
      this.onDoubleClickHandler(event);
    });
  }

  onDoubleClickHandler(event: MouseEvent) {
    this.drawingEventHandler(
      new StartDrawingTextEvent(
        { position: this.normalisePosition(event) },
        { x: event.pageX, y: event.pageY }
      )
    );
  }

  onMouseUpHandler(event: MouseEvent) {
    this.mouseIsDown = false;
    const currentTime = new Date().getTime();
    const tapLength = currentTime - this.lastTapTime;
    if (tapLength < DOUBLE_CLICK_MAX_INTERVAL && tapLength > 0) {
      event.preventDefault();
      this.internalEventPublisher.publish('doubleClick', event);
    }
    this.lastTapTime = currentTime;
  }

  formDrawPointEvent(event: MouseEvent) {
    return new DrawPointEvent({
      position: this.normalisePosition(event),
    });
  }

  normalisePosition(event: MouseEvent) {
    return {
      x: (event.pageX - this.canvas.offsetLeft) / this.canvas.clientWidth,
      y: (event.pageY - this.canvas.offsetTop) / this.canvas.clientHeight,
    };
  }

  onMouseDownHandler(event: MouseEvent) {
    this.mouseIsDown = true;
    let firstPointDrawn = false;
    const firstEvent = event;
    const eventPublisher = new SimpleEventPublisher();

    const onMouseMoveHandler = (event: any) => {
      if (!firstPointDrawn) {
        this.drawingEventHandler(
          this.formDrawPointEvent(firstEvent)
        );
        firstPointDrawn = true;
        eventPublisher.publish('drawingStarted', firstEvent);
      }
      this.drawingEventHandler(
        this.formDrawPointEvent(event)
      );
    };

    const cancelDrawingMouseUpHandler = (event: MouseEvent) => {
      this.canvas.removeEventListener('mousemove', onMouseMoveHandler);
    };
    this.canvas.addEventListener('mouseup', cancelDrawingMouseUpHandler);

    if (this.mouseIsDown) {
      this.canvas.addEventListener('mousemove', onMouseMoveHandler);
    }

    this.internalEventPublisher.addSubscriber(
      'doubleClick',
      () => this.canvas.removeEventListener('mousemove', onMouseMoveHandler)
    );

    const finishDrawingMouseUpHandler = (event: MouseEvent) => {
      this.canvas.removeEventListener('mousemove', onMouseMoveHandler);
      this.drawingEventHandler(
        new StopDrawingEvent({
          position: this.normalisePosition(event),
        })
      );
      eventPublisher.publish('mouseUpForDrawingApplied');
    };

    eventPublisher.addSubscriber(
      'mouseUpForDrawingApplied',
      () => this.canvas.removeEventListener('mouseup', finishDrawingMouseUpHandler)
    );

    // this.canvas.addEventListener('mouseup', mouseUpHandler);
    eventPublisher.addSubscriber(
      'drawingStarted',
      () => {
        this.canvas.removeEventListener('mousemove', cancelDrawingMouseUpHandler);
        this.canvas.addEventListener('mouseup', finishDrawingMouseUpHandler);
      }
    );
  }
}
