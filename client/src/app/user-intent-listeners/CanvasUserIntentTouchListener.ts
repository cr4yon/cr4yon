import {
  DrawPointEvent,
  StopDrawingEvent,
  DrawingEvent,
  StartDrawingTextEvent,
} from 'models';
import SimpleEventPublisher from './SimpleEventPublisher';

interface Touch {
  x: number;
  y: number;
  pageX: number;
  pageY: number;
}

interface TouchEvent {
  touches: Touch[];
  changedTouches: Touch[];
  preventDefault: Function;
}

const DOUBLE_TAP_MAX_INTERVAL = 300;

export default class CanvasUserIntentTouchListener {
  canvas: any;
  drawingEventHandler: (drawingEvent: DrawingEvent) => any;
  lastTapTime: number;
  lastTouchStartTapTime: number;
  internalEventPublisher: SimpleEventPublisher;

  constructor(canvas: any, drawingEventHandler: (drawingEvent: DrawingEvent) => any) {
    this.canvas = canvas;
    this.drawingEventHandler = drawingEventHandler;
    this.internalEventPublisher = new SimpleEventPublisher();
  }

  start() {
    this.setUpDomEventListeners();
    this.setUpInternalEventSubscribers();
  }

  setUpDomEventListeners() {
    this.canvas.addEventListener('touchstart', (event: TouchEvent) => {
      this.onTouchStartForDrawingHandler(event);
      this.onTouchStartForDoubleTapHandler(event);
    });
    this.canvas.addEventListener('touchend', (event: TouchEvent) => {
      this.onTouchEndForDoubleTapHandler(event);
    });
  }

  // Duplicate!!
  onTouchStartForDoubleTapHandler(event: TouchEvent) {
    const currentTime = new Date().getTime();
    const tapLength = currentTime - this.lastTouchStartTapTime;
    if (tapLength < DOUBLE_TAP_MAX_INTERVAL && tapLength > 0) {
      event.preventDefault();
      this.internalEventPublisher.publish('doubleTap', event);
    }
    this.lastTouchStartTapTime = currentTime;
  }

  onTouchEndForDoubleTapHandler(event: TouchEvent) {
    const currentTime = new Date().getTime();
    const tapLength = currentTime - this.lastTapTime;
    if (tapLength < DOUBLE_TAP_MAX_INTERVAL && tapLength > 0) {
      event.preventDefault();
      this.internalEventPublisher.publish('doubleTap', event);
    }
    this.lastTapTime = currentTime;
  }

  setUpInternalEventSubscribers() {
    this.internalEventPublisher.addSubscriber('doubleTap', (event: TouchEvent) => {
      this.onDoubleTapHandler(event);
    });
  }

  onDoubleTapHandler(event: TouchEvent) {
    const touches = event.touches.length > 0 ? event.touches : event.changedTouches;
    const firstTouch = touches[0];
    this.drawingEventHandler(
      new StartDrawingTextEvent(
        { position: this.normalisePosition(firstTouch) },
        { x: firstTouch.pageX, y: firstTouch.pageY }
      )
    );
  }

  formDrawPointEvent(event: TouchEvent) {
    return new DrawPointEvent({
      position: this.normalisePosition(event.touches[0]),
    });
  }

  normalisePosition(touch: Touch) {
    return {
      x: (touch.pageX - this.canvas.offsetLeft) / this.canvas.clientWidth,
      y: (touch.pageY - this.canvas.offsetTop) / this.canvas.clientHeight,
    };
  }

  onTouchStartForDrawingHandler(event: TouchEvent) {
    event.preventDefault();
    let firstPointDrawn = false;
    const firstEvent = event;

    const onTouchMoveHandler = (event: any) => {
      event.preventDefault();
      if (!firstPointDrawn) {
        this.drawingEventHandler(
          this.formDrawPointEvent(firstEvent)
        );
        firstPointDrawn = true;
        eventPublisher.publish('drawingStarted', firstEvent);
      }
      this.drawingEventHandler(
        this.formDrawPointEvent(event)
      );
    };

    const cancelDrawingTouchEndHandler = (event: TouchEvent) => {
      this.canvas.removeEventListener('touchmove', onTouchMoveHandler);
    };
    this.canvas.addEventListener('touchend', cancelDrawingTouchEndHandler);

    this.canvas.addEventListener('touchmove', onTouchMoveHandler);

    this.internalEventPublisher.addSubscriber(
      'doubleTap',
      () => this.canvas.removeEventListener('touchmove', onTouchMoveHandler)
    );

    const eventPublisher = new SimpleEventPublisher();

    const onTouchEndHandler = (event: any) => {
      this.canvas.removeEventListener('touchmove', onTouchMoveHandler);
      const touches = event.touches.length > 0 ? event.touches : event.changedTouches;
      this.drawingEventHandler(
        new StopDrawingEvent({
          position: this.normalisePosition(touches[0]),
        })
      );
      eventPublisher.publish('touchEndForDrawingApplied');
    };

    eventPublisher.addSubscriber(
      'touchEndForDrawingApplied',
      () => this.canvas.removeEventListener('touchend', onTouchEndHandler)
    );

    eventPublisher.addSubscriber(
      'drawingStarted',
      () => this.canvas.addEventListener('touchend', onTouchEndHandler)
    );
  }
}
