export default class SimpleEventPublisher {
  subcsribers: {
    eventName: string,
    eventConsumer: (event: any) => void
  }[];

  constructor() {
    this.subcsribers = [];
  }

  addSubscriber(eventName: string, eventConsumer: (event?: any) => void) {
    this.subcsribers.push({ eventName, eventConsumer });
  }

  publish(eventName: string, event?: any) {
    this.subcsribers
      .filter(s => s.eventName === eventName)
      .forEach(s => s.eventConsumer(event));
  }
}
