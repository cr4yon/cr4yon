import { roomStateLoaded$ } from './observables';
import { roomStateLoadedSubscriber } from './observable-subscribers';

export default subscribeToStateObservables;

function subscribeToStateObservables(store: any) {
  roomStateLoaded$(store).subscribe(roomStateLoadedSubscriber(store));
}
