import { Point, DrawingEvent, DrawingEventType, Position } from 'models';
import { RootState, Room } from 'state';

export interface Action {
  type: string;
  [propName: string]: any;
}

interface ActionDefinition {
  type: string;
  creator: (...inputs: any[]) => Action;
}

export const START_DRAWING_TEXT = definition(
  'START_DRAWING_TEXT',
  (point: Point, pagePosition: Position) => {
    return { point, pagePosition };
  }
);

const HANDLE_LOCAL_DRAWING_EVENT_TYPE = 'HANDLE_LOCAL_DRAWING_EVENT';
export const HANDLE_LOCAL_DRAWING_EVENT = {
  type: HANDLE_LOCAL_DRAWING_EVENT_TYPE,
  creator: (type: DrawingEventType, point: Point, details: any) => {
    return (dispatch: (action: Action) => any, getState: () => RootState) :any => {
      return new Promise((resolve, reject) => {
        const room = getState().room;
        if (!room) reject('User has not joined any room');

        const drawingEvent = {
          type,
          details,
          point: {
            ...point,
            user: getState().authentication.userName,
          },
          roomName: getState().room!.name, // should not be needed
        };
        const action = {
          type: HANDLE_LOCAL_DRAWING_EVENT_TYPE,
          event: drawingEvent,
        };
        dispatch(action);
        const socket = getState().socket;
        socket.emit('drawing-event', drawingEvent);
        resolve();
      });
    };
  },
};

const JOIN_WHITEBOARD_DEF = definition(
  'JOIN_WHITEBOARD',
  // tslint:disable-next-line
  (userName: String, roomName: String, result?: String, message?: String) => ({ userName, roomName, result, message })
);
export const JOIN_WHITEBOARD = {
  type: JOIN_WHITEBOARD_DEF.type,
  creator: (userName: String, roomName: String) => {
    return (dispatch: (action: Action) => any, getState: () => RootState) :any => {
      return new Promise((resolve, reject) => {
        const action = JOIN_WHITEBOARD_DEF.creator(userName, roomName);
        dispatch(action);
        console.log('state', getState());
        const socket = getState().socket;
        console.log('socket to join', socket);
        socket.emit('join-whiteboard', action);

        socket.on('join-whiteboard', (msg: any) => {
          console.log('received reply on socket', msg);
          if (msg.result === 'DECLINED') {
            dispatch(
              JOIN_WHITEBOARD_DEF.creator(userName, roomName, 'DECLINED', msg.declineReason)
            );
          } else {
            dispatch(
              JOIN_WHITEBOARD_DEF.creator(userName, roomName, 'ACCEPTED')
            );
          }
        });
      });
    };
  },
};

export const RECEIVE_DRAWING_EVENT = definition(
  'RECEIVE_DRAWING_EVENT',
  (event: DrawingEvent) => ({ event })
);

export interface DrawingState {
  room: Room;
  points: Point[];
}
export const DRAWING_STATE_RECEIVED = definition(
  'DRAWING_STATE_RECEIVED',
  (drawingState: DrawingState) => ({ drawingState })
);

export const ROOM_STATE_RECEIVED = definition(
  'ROOM_STATE_RECEIVED',
  (room: Room) => ({ room })
);

export const HANDLE_DISCONNECTION = definition(
  'HANDLE_DISCONNECTION',
  () => ({})
);

export const CHOOSE_COLOR = definition('CHOOSE_COLOR', (color: String) => ({ color }));

export const CHOOSE_LINE_WIDTH = definition(
  'CHOOSE_LINE_WIDTH',
  (lineWidth: number) => ({ lineWidth })
);


function definition(type: string, partialCreator?: Function) :ActionDefinition {
  return {
    type,
    creator: (...args: any[]) => (partialCreator ? { ...partialCreator(...args), type } : { type }),
  };
}
