const webpackBaseConfig = require('./webpack.base-config');

module.exports = Object.assign(
  {},
  webpackBaseConfig,
  {
    output: Object.assign(
      {},
      webpackBaseConfig.output
    ),
    devServer: {
      contentBase: 'public',
      hot: true,
      compress: true,
      historyApiFallback: true,
      open: true,
      proxy: [
        {
          context: ['/api/**'],
          target: process.env.WEBAPP_URL || 'https://test.transferwise.com',
          secure: false,
          changeOrigin: true,
          headers: {
            'X-Authorization-key': 'ypfac431vbs3qs98772vxvxc923vzv19',
          },
        },
      ],
    },
  }
);
