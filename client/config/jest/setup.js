global.requestAnimationFrame = (callback) => {
  setTimeout(callback, 0);
};

const Enzyme = require('enzyme');
const Adapter = require('enzyme-adapter-react-16');
Enzyme.configure({ adapter: new Adapter() });

const $ = require('jquery');
window.$ = window.jQuery = $;

const _ = require('underscore');
window._ = _;
