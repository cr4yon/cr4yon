const webpack = require('webpack');
const webpackBaseConfig = require('./webpack.base-config')

module.exports = Object.assign(
  {},
  webpackBaseConfig,
  {
    output: Object.assign(
      {},
      webpackBaseConfig.output,
      { path: __dirname + "/dist" }
    ),
  },
  {
    plugins: webpackBaseConfig.plugins.concat([
      new webpack.optimize.UglifyJsPlugin({
        parallel: true,
        mangle: {
          keep_fnames: true,
        },
      }),
    ]),
  }
);
