declare module '@transferwise/components';
declare module '@transferwise/twcard-components';
declare module '@transferwise/twcard-components/src';
declare module '@transferwise/questionable/context-aware-faqs';
declare module "*.json" {
  const value: any;
  export default value;
}
declare module '*.svg';
declare module '*.png';
declare module 'jest-fetch-mock';
declare module 'urldecode';
declare module 'socket.io-client';
declare module 'enzyme';
declare module 'prop-types';
declare module 'react-autobind';
declare module 'react-bootstrap';
declare module 'react-redux';
declare module 'react-router-dom';
declare module 'redux-rx';
declare module 'redux-logger';
declare module 'react-test-renderer';
declare module 'react-color';
declare module '@transferwise/navigation/application';
