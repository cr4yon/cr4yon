const ExtractTextPlugin = require("extract-text-webpack-plugin");
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");
const TsConfigPathsPlugin = require('awesome-typescript-loader').TsConfigPathsPlugin;
const webpack = require('webpack');
const path = require('path');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const appConfig = require('./app-config');

module.exports = {
  entry: [
    './src/public-path.js',
    require.resolve('./config/polyfills'),
    './src/index.tsx',
  ],
  output: {
    filename: `${appConfig.appName}.min.js`,
    path: __dirname + "/dist"
  },
  devtool: "source-map",
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".jsx", ".json", ".css", ".less"],
    descriptionFiles: ["package.json", "tsconfig.json", "tsconfig.test.json", "tslint.json"],
    alias: {
      "i18n": path.resolve(__dirname, 'src/app/i18n'),
      "actions": path.resolve(__dirname, 'src/app/actions'),
      "config": path.resolve(__dirname, 'src/config'),
      "components": path.resolve(__dirname, 'src/app/components'),
      "pages": path.resolve(__dirname, 'src/app/pages'),
      "clients": path.resolve(__dirname, 'src/app/clients'),
      "flows": path.resolve(__dirname, 'src/app/flows'),
      "models": path.resolve(__dirname, 'src/app/models'),
      "reducers": path.resolve(__dirname, 'src/app/reducers'),
      "state": path.resolve(__dirname, 'src/app/state'),
      "drawers": path.resolve(__dirname, 'src/app/drawers'),
      "observables": path.resolve(__dirname, 'src/app/observables'),
      "observable-subscribers": path.resolve(__dirname, 'src/app/observable-subscribers'),
      "user-intent-listeners": path.resolve(__dirname, 'src/app/user-intent-listeners'),
      "socket-event-subscribers": path.resolve(__dirname, 'src/app/socket-event-subscribers'),
      "styles": path.resolve(__dirname, 'src/styles')
    }
  },
  module: {
    rules: [
      // ts-loader: convert typescript (es6) to javascript (es6),
      // babel-loader: converts javascript (es6) to javascript (es5)
      {
        test: /\.tsx?$/,
        loaders: ['babel-loader','ts-loader'],
        exclude: [
          /node_modules/,
          'src/test-utils',
        ],
      },
      // babel-loader for pure javascript (es6) => javascript (es5)
      {
        test: /\.(jsx?)$/,
        loaders: ['babel-loader'],
        exclude: [/node_modules/],
      },
      { test: /\.css$/, loader: ExtractTextPlugin.extract("css-loader") },
      { test: /\.less$/, exclude: /node_modules/, loader: ExtractTextPlugin.extract("css-loader!less-loader") },
      { test: /\.(woff|woff2|eot|ttf)/, loader: 'file-loader?name=fonts/[name].[ext]' },
      // {
      //   test: /\.(jpe?g|png|gif)$/i,
      //   use: [
      //     'url-loader?limit=10000',
      //     'img-loader'
      //   ]
      // },
      {
        test: /\.svg(\?.+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              mimetype: 'image/svg+xml',
            },
          },
        ],
      },
      {
        test: /\.png(\?.+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 200000,
              mimetype: 'image/png',
            },
          },
        ],
      }
    ]
  },
  plugins: [
	  new ExtractTextPlugin(`${appConfig.appName}.min.css`),
    new OptimizeCssAssetsPlugin(),
    new webpack.ProvidePlugin({
      Promise: 'es6-promise-promise'
    }),
    new webpack.ProvidePlugin({
      _: 'underscore',
      $: 'jquery',
      jQuery: 'jquery',
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      'process.env.API_URL': JSON.stringify(process.env.API_URL),
    }),
    new TsConfigPathsPlugin({
      configFileName: './tsconfig.json',
      compiler: 'typescript'
    })
  ],
};
