# Crayon
Drawing like there's no tomorrow!
This project is built in React and TypeScript.

## Installing
With `npm`:
run `npm install`

## Testing
Testing is done with [Jest](https://facebook.github.io/jest/)

With `npm`:
run `npm test` or `npm run test:dev` for watch mode

## Running
With `npm`:
run `npm start`

With https:
run `npm start -- --https`

## Building
The `/dist` file is ignored by git but this file is generated when running `yarn build` (or `npm run build`) and contains the minified bundle of transpiled JavaScript code and minified and compiled css code that can be easily imported into other projects.

The bundling is done by Webpack and it uses different loaders to bundle and transpile different file types:
- `awesome-typescript-loader-loader` for typescript files
- `css-loader` for css files
- `less-loader` for less files

Webpack is configured to get all relevant files apart from the configs, and node_modules, so inherently it will bundle everything in the `src` directory

There are plugins installed for minifying the outputted files (both js and css).
