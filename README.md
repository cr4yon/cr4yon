# Cr4yon
- v1 at http://cr4yon.com
- wip v2 at http://cr4yon.com:8081

This side-project is not being worked on anymore.

It is not production ready nor really developer friendly even if some efforts were put in code and tests.
Self-help instructions are mostly missing.

Some interesting results when it was first published:
- [Product Hunt post](https://www.producthunt.com/posts/cr4yon)

## Run
```
docker-compose -f $theFileYouWant up
```
Then go to http://localhost:80

### API server running on port :3000
http://cr4yon.com:3000

### Prometheus server running on port :9090
One can go to http://cr4yon.com:9090/metrics

## Monitor
Basic Prometheus and Grafana setup + node-exporter
One can visit Grafana on http://cr4yon.com:3001 with known credentials.

Still much to do there.

### TODO
- [ ] Provision Grafana from config instead of ad-hoc from UI

## Data store
The default is in memory (`ROOM_STORE_TYPE=inMemory`), so a server restart will remove all data.
Optionally one can use Redis by passing the following env variables:
- `ROOM_STORE_TYPE=redis`
- `REDIS_HOST=`
- `REDIS_PORT=`

## v2 is WIP
One can access it on port `:8081`:
http://cr4yon.com:8081

### What was v2 work about?
- Better UX
- Tool palette to go for a plugin approach later
  - Idea was you could even drop JS snippets to add tools on the page
- Better handling of events

## Deployment
Set the .env variables in the Digital Ocean instance to the docker tags you're interested in.
These tags come from the automatic builds triggered by a `master` update.

One has to manually ssh to the DO insteance, update `.env` with the wanted tags, and restart the docker compose (I know... could be better).