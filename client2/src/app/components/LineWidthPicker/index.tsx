import * as React from 'react';
import { connect } from 'react-redux';
import { RootState } from '../../state';
import { Action, CHOOSE_LINE_WIDTH } from '../../actions';
import {ChangeEvent, FormEvent} from "react";

const mapStateToProps = (state: RootState, ownProps: {}): PropsFromState => {
  return {
    ...ownProps,
    currentLineWidth: state.palette.lineWidth,
    sampleColor: state.palette.color,
  };
};

const mapDispatchToProps = (dispatch: (action: Action) => any,
                            ownProps: {}): PropsFromDispatch => {
  return {
    ...ownProps,
    chooseLineWidth: (lineWidth: number): void => dispatch(CHOOSE_LINE_WIDTH.creator(lineWidth)),
  };
};

interface PropsFromState {
  currentLineWidth: number;
  sampleColor: string;
}

interface PropsFromDispatch {
  chooseLineWidth: (lineWidth: number) => any;
}

type Props = PropsFromState  & PropsFromDispatch;

interface State {
  inputValue: number;
}

class LineWidthPicker extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { inputValue: props.currentLineWidth };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event: ChangeEvent<HTMLInputElement>): void {
    const target = event.target as HTMLInputElement
    const val = Number(target.value)
    this.setState({ inputValue: val });
    this.props.chooseLineWidth(val);
  }

  handleSubmit(event: FormEvent<HTMLFormElement>): void {
    event.preventDefault();
  }

  isInputValueValid(): boolean {
    return !!this.state.inputValue;
  }

  render(): JSX.Element {
    const sampleStyles = {
      height: `${this.state.inputValue}px`,
      width: `${Math.min(4,this.state.inputValue)}px`,
      backgroundColor: this.props.sampleColor,
      display: 'inline-block',
    };
    return (
      <form onSubmit={this.handleSubmit}>
        <div
          style={{
            width: '100%',
            display: 'grid',
            gridTemplateColumns: `10fr 2fr`,
            alignItems: 'center',
            columnGap: '6px',
          }}
        >
          <div>
            <div
              className="input-group"
              style={{ width: '100%' }}
            >
              <input
                style={{ width: '100%' }}
                id="chooseLineWidthInput"
                type="range"
                min="1"
                max="50"
                step="1"
                value={this.state.inputValue}
                onChange={this.handleChange}
                list="widthOptions"
              />
              <datalist id="widthOptions">
                <option value="1" label="1px"/>
                <option value="2"/>
                <option value="3" label="3px"/>
                <option value="5"/>
                <option value="8" label="8px"/>
                <option value="13"/>
                <option value="21" label="21px"/>
                <option value="33"/>
                <option value="54" label="54px"/>
                <option value="87"/>
              </datalist>
            </div>
          </div>
          <div>
            <div className="input-group-text">
              <div style={sampleStyles}/>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

export { LineWidthPicker };
export default connect(mapStateToProps, mapDispatchToProps)(LineWidthPicker);
