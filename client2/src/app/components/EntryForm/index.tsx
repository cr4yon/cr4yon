import decode from 'urldecode';
import * as React from 'react';
import { connect } from 'react-redux';

import { Action, JOIN_WHITEBOARD } from '../../actions';
import { RootState } from '../../state';
import {ChangeEvent, FormEvent} from "react";

import './index.scss';

const mapStateToProps = (state: RootState, ownProps: {}): {} => {
  return {
    ...ownProps,
  };
};

const mapDispatchToProps = (dispatch: (action: any) => any,
                            ownProps: {}): Props => {
  return {
    ...ownProps,
    joinWhiteBoard: (userName: string, roomName: string) => {
      dispatch(JOIN_WHITEBOARD.creator(userName, roomName));
    },
  };
};

interface Props {
  joinWhiteBoard: (userName: string, roomName: string) => any;
}

interface State {
  userNameInputValue: string;
  roomNameInputValue: string;
}

function getURLParameter(sParam: string): string | null{
  const pageURL = window.location.search.substring(1);
  const urlVariables = pageURL.split('&');
  const parameter = urlVariables.find(v => v.split('=')[0] === sParam);

  return parameter ? parameter.split('=')[1] : null;
}

class EntryForm extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { userNameInputValue: '', roomNameInputValue: '' };

    this.handleUserNameChange = this.handleUserNameChange.bind(this);
    this.handlRoomNameChange = this.handlRoomNameChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillMount(): void {
    const roomNameFromUrlParams = getURLParameter('room');
    if (roomNameFromUrlParams && this.isNonEmptyString(roomNameFromUrlParams)) {
      this.setState({ roomNameInputValue: decode(roomNameFromUrlParams) });
    }
  }

  handleUserNameChange(event: ChangeEvent<HTMLInputElement>): void {
    this.setState({ userNameInputValue: event.target.value });
  }

  handlRoomNameChange(event: ChangeEvent<HTMLInputElement>): void {
    this.setState({ roomNameInputValue: event.target.value });
  }

  handleSubmit(event: FormEvent<HTMLFormElement>): void {
    console.log('Trying to join room');
    this.props.joinWhiteBoard(this.state.userNameInputValue, this.state.roomNameInputValue);
    event.preventDefault();
  }

  isFormValid(): boolean {
    return this.isNonEmptyString(this.state.userNameInputValue) &&
     this.isNonEmptyString(this.state.roomNameInputValue);
  }

  isNonEmptyString(str: string | null | undefined): boolean {
    return str !== null && str !== undefined && str !== '';
  }

  render(): JSX.Element {
    return (
      <form id="entryForm" onSubmit={this.handleSubmit}>

        <div className="input-group">
          <div className="m-b-1">
            <label htmlFor="chooseUserNameInput" className="m-b-1">
              Your name
            </label>
          </div>
          <div>
            <input
              id="chooseUserNameInput"
              type="text"
              value={this.state.userNameInputValue}
              onChange={this.handleUserNameChange}
            />
          </div>
        </div>

        <div className="input-group">
          <div className="m-b-1">
            <label htmlFor="chooseRoomNameInput">
              Room to join
            </label>
          </div>
          <div>
            <input
              id="chooseRoomNameInput"
              type="text"
              value={this.state.roomNameInputValue}
              onChange={this.handlRoomNameChange}
            />
          </div>
        </div>

        <div className="input-group">
          <div style={{ textAlign: 'right'}}>
            <button
              type="submit"
              disabled={!this.isFormValid()}
            >
              Join
            </button>
          </div>
        </div>
      </form>
    );
  }
}

// TODO https://stackoverflow.com/questions/41808408/react-redux-typescript-connect-has-missing-type-error
export { EntryForm };
export default connect<{}, Props, {}, RootState>(mapStateToProps, mapDispatchToProps)(EntryForm);
