import * as React from 'react';
import { Action } from '../../actions';
import { connect } from 'react-redux';
import { RootState } from '../../state';

const mapStateToProps = (state: RootState, ownProps: {}): Props => {
  return {
    ...ownProps,
    error: state.error,
  };
};

const mapDispatchToProps = (dispatch: (action: Action) => any,
                            ownProps: {}): Partial<Props> => {
  return {
    ...ownProps,
  };
};

interface Props {
  error: string | undefined;
}

function GlobalError({error}: Props): JSX.Element | null {
    return error ? (
        <div className="alert alert-danger">
          <p> {error} </p>
        </div>
      ) : null;
}

export { GlobalError };
export default connect(mapStateToProps, mapDispatchToProps)(GlobalError);
