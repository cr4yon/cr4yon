import * as React from 'react';
import image from './crayon.svg';

interface Props {
  maxHeight: string;
}

export default class CrayonImage extends React.PureComponent<Props> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <img
        src={image}
        style={{
          maxHeight: this.props.maxHeight || '100px',
        }}
      />
    );
  }
}
