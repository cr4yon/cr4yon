import * as React from 'react';
import CrayonImage from '../CrayonImage';

interface Props {
  maxHeight: string;
}

class LogoWithText extends React.PureComponent<Props> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <div>
        <div style={{ float: 'left', display: 'inline-block' }}>
          <CrayonImage maxHeight={this.props.maxHeight} />
          <span className="badge badge-info badge-sm">&#945;</span>
        </div>
        <div style={{ display: 'inline-block' }}>
          <h2> Crayon </h2>
        </div>
      </div>
    );
  }
}

export default LogoWithText;
