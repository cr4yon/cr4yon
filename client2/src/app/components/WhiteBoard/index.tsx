import * as React from 'react';
import {connect, MapStateToProps} from 'react-redux';
// should separate models per feature: drawer, userIntentListener, etc...
import {Drawer} from '../../drawers/interfaces';
import {default as CanvasDrawer2D} from '../../drawers/CanvasDrawer2D';
import {
  ContinueDrawingTextEvent,
  DrawingEvent,
  DrawingEventType,
  DrawPointStrategy,
  DrawTextEvent,
  Point,
  Position,
  StartDrawingTextEvent, Tool,
} from '../../models';
import {AppThunkDispatch, HANDLE_LOCAL_DRAWING_EVENT, START_DRAWING_TEXT,} from '../../actions';
import {UserIntentListener,} from '../../user-intent-listeners/interfaces';
import {default as CanvasUserIntentMouseListener,} from '../../user-intent-listeners/CanvasUserIntentMouseListener';
import {default as CanvasUserIntentTouchListener,} from '../../user-intent-listeners/CanvasUserIntentTouchListener';
import {CurrentSelection, DrawingActionType, DrawingHistoricalEvent, Palette, RootState} from '../../state';
import CanvasUserIntentTextMouseListener from "../../user-intent-listeners/CanvasUserIntentTextMouseListener";
import CanvasUserIntentTextTouchListener from "../../user-intent-listeners/CanvasUserIntentTextTouchListener";

interface PublicProps {
  contextType: string;
  width: number;
  height: number;
}

const mapStateToProps: MapStateToProps<ValueProps, PublicProps, RootState> = (state: RootState, ownProps: PublicProps): ValueProps => {
  const toolMap = new(Map);
  state.tools.forEach(t => toolMap.set(t.id, t))
  return {
    ...ownProps,
    isDrawing: state.drawingState.drawingActionType === DrawingActionType.POINT
      && state.drawingState.inProgress,
    drawingHistory: state.drawingHistory,
    color: state.color,
    palette: state.palette,
    whiteBoardFillingColor: '#ffffff',
    currentSelection: state.currentSelection,
    tools: toolMap,
  };
};

const mapDispatchToProps = (dispatch: AppThunkDispatch, ownProps: {}): ActionProps => {
  return {
    ...ownProps,
    drawPoint: (point: Point) => {
      dispatch(
        HANDLE_LOCAL_DRAWING_EVENT.creator(DrawingEventType.DRAW_POINT, point, {})
      );
    },
    stopDrawing: (point: Point) => {
      dispatch(
        HANDLE_LOCAL_DRAWING_EVENT.creator(DrawingEventType.STOP_DRAWING, point, {})
      );
    },
    startDrawingText: (point: Point, pagePosition: Position) => {
      dispatch(
          HANDLE_LOCAL_DRAWING_EVENT.creator(DrawingEventType.START_DRAWING_TEXT, point, {})
      );
    },
    continueDrawingText: (point: Point, newValue: string) => {
      dispatch(
          HANDLE_LOCAL_DRAWING_EVENT.creator(DrawingEventType.CONTINUE_DRAWING_TEXT, point, {newValue})
      );
    },
    drawText: (point: Point, textValue: string) => {
      dispatch(
          HANDLE_LOCAL_DRAWING_EVENT.creator(DrawingEventType.DRAW_TEXT, point, { textValue })
      );
    },
  };
};

interface ValueProps {
  isDrawing: boolean;
  contextType: string;
  width: number;
  height: number;
  drawingHistory: DrawingHistoricalEvent[];
  color: string;
  whiteBoardFillingColor?: string;
  palette: Palette;
  currentSelection: CurrentSelection,
  tools: Map<string,Tool>,
}

interface ActionProps {
  drawPoint: (point: Point) => any;
  stopDrawing: (point: Point) => any;
  startDrawingText: (point: Point, pagePosition: Position) => any;
  continueDrawingText: (point: Point, newValue: string) => any;
  drawText: (point: Point, textValue: string) => any;
}

interface Props extends ValueProps, ActionProps {}

class WhiteBoard extends React.Component<Props, any> {
  canvas: HTMLCanvasElement | null;
  overlay: HTMLDivElement | null;
  context: any;
  drawer: Drawer | undefined;
  otherDrawers: Map<string, Drawer>;
  userIntentListeners: Map<string,UserIntentListener[]>;

  constructor(props: Props) {
    super(props);
    console.info("Context", props.contextType);
    this.canvas = null;
    this.overlay = null;
    this.otherDrawers = new(Map)
    this.userIntentListeners = new(Map)
  }

  gDrawer(): Drawer {
    if (this.drawer === undefined) {
      throw Error("Drawer not loaded")
    }
    return this.drawer
  }

  gCanvas(): HTMLCanvasElement {
    if (this.canvas === null) {
      throw Error("Canvas not loaded")
    }
    return this.canvas
  }

  gOverlay(): HTMLDivElement {
    if (this.overlay === null) {
      throw Error("Overlay not loaded")
    }
    return this.overlay
  }

  componentDidMount() {
    this.context = this.getContextOrError();

    this.drawer = this.loadDrawer();
    this.otherDrawers = new Map();

    if (this.props.whiteBoardFillingColor) {
      this.fillCanvasBackground();
    }
    this.drawHistory(this.props.drawingHistory);

    this.userIntentListeners.set('pen', [
      new CanvasUserIntentTouchListener(
        this.gOverlay(),
        this.getDrawingEventHandler()
      ),
      new CanvasUserIntentMouseListener(
        this.gOverlay(),
        this.getDrawingEventHandler(),
          0.015,
      ),
    ]);

    this.userIntentListeners.set('text', [
      new CanvasUserIntentTextMouseListener(
          this.gOverlay(),
          this.getDrawingEventHandler()
      ),
      new CanvasUserIntentTextTouchListener(
          this.gOverlay(),
          this.getDrawingEventHandler()
      ),
    ])

    console.info("Current selection", this.props.currentSelection);

    this.userIntentListeners.forEach((listeners, tool) => {
      if (this.props.currentSelection.toolId === tool) {
        console.info("starting listeners for tool", tool);
        listeners.forEach(l => l.start())
      }
    });
  }

  fillCanvasBackground() {
    this.gDrawer().fillCanvasBackground(this.props.whiteBoardFillingColor || '#fff');
  }

  eraseAll() {
    this.gDrawer().clean();
  }

  loadDrawer(): Drawer {
    switch (this.props.contextType) {
      case '2d':
      default:
        return new CanvasDrawer2D(this.gCanvas(), this.gOverlay(), this.getContextOrError() as CanvasRenderingContext2D, DrawPointStrategy.LINE);
    }
  }

  getDrawingEventHandler() {
    return (drawingEvent: DrawingEvent) => {
      switch (drawingEvent.type) {
        case DrawingEventType.DRAW_POINT:
          this.props.drawPoint(
            {
              ...drawingEvent.point,
              color: this.props.color,
              lineWidth: this.props.palette.lineWidth,
            }
          );
          return;
        case DrawingEventType.STOP_DRAWING:
          this.props.stopDrawing(drawingEvent.point);
          return;
        case DrawingEventType.START_DRAWING_TEXT:
          this.props.startDrawingText(
            drawingEvent.point,
            (drawingEvent as StartDrawingTextEvent).absolutePagePosition
          );
          return;
        case DrawingEventType.CONTINUE_DRAWING_TEXT:
          this.props.continueDrawingText(
              drawingEvent.point,
              (drawingEvent as ContinueDrawingTextEvent).newValue
          );
          return;
        case DrawingEventType.DRAW_TEXT:
          this.props.drawText(
              drawingEvent.point,
              (drawingEvent as DrawTextEvent).text
          );
          return;
        default:
          throw new Error(`Cannot handle drawing event type ${drawingEvent.type}`);
      }
    };
  }

  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.isDrawing !== this.props.isDrawing) {
      if (!nextProps.isDrawing) {
        this.stopDrawing();
      }
    }

    if (nextProps.drawingHistory !== this.props.drawingHistory) {
      this.drawNewHistory(nextProps);
    }

    // Use map of toolId => UserIntentListeners
    const toolId = nextProps.currentSelection.toolId
    this.userIntentListeners.forEach((listeners, tool) => {
      if (tool !== toolId) {
        console.info(`Stopping user intent listeners for tool ${toolId}`);
        listeners.forEach((l) => {
          l.stop();
        })
      } else {
        console.info(`Starting user intent listeners for tool ${toolId}`);
        listeners.forEach((l) => {
          l.start();
        })
      }
    });

    this.gOverlay().style.cursor = this.decideOverlayCursor(nextProps);
  }

  decideOverlayCursor(props: Props): string {
    const toolId = props.currentSelection.toolId
    const tool = props.tools.get(toolId);
    return tool ? tool.cursor : 'pointer';
  }

  stopDrawing() {
    this.gDrawer().stopDrawing();
  }

  redrawAllPoints() {
    this.eraseAll();
    this.fillCanvasBackground();
    this.drawHistory(this.props.drawingHistory);
  }

  drawNewHistory(nextProps: Props) {
    if (nextProps.drawingHistory.length > this.props.drawingHistory.length) {
      this.drawHistory(nextProps.drawingHistory.slice(this.props.drawingHistory.length));
    } else {
      this.eraseAll();
      this.fillCanvasBackground();
      this.drawHistory(nextProps.drawingHistory);
    }
  }

  drawHistory(historicalEvents: DrawingHistoricalEvent[]) {
    console.log("drawHistory called, total=", historicalEvents.length)
    console.log(historicalEvents);
    historicalEvents.forEach((event) => {
      const drawTextCommand = {
        position: event.point.position,
        color: event.point.color || '#000000', // fix
        lineWidth: event.point.lineWidth || 2, // fix
        textValue: event.details.textValue,
      };
      let drawer = this.gDrawer();
      if (event.point.user) {
        const userName = event.point.user;
        if (!this.otherDrawers.has(userName)) {
          this.otherDrawers.set(userName, this.loadDrawer());
        }
        drawer = this.otherDrawers.get(userName)!;
      }
      // TODO Avoid the switch and give event to drawer
      switch (event.type) {
        case DrawingEventType.START_DRAWING_TEXT:
          drawer.startDrawText(event.point.position)
          return
        case DrawingEventType.CONTINUE_DRAWING_TEXT:
          const newValue = event.details.newValue;
          drawer.continueDrawingText(newValue);
          return
        case DrawingEventType.DRAW_TEXT:
          drawer!.drawText(drawTextCommand);
          return;
        case DrawingEventType.STOP_DRAWING:
          drawer!.stopDrawing();
          return;
        case DrawingEventType.DRAW_POINT:
          drawer!.drawPoint(event.point);
          return;
        default:
          console.error('No handling of historical event', event.type);
      }
    });
  }

  shouldComponentUpdate() {
    return false;
  }

  getContextOrError(): CanvasRenderingContext2D | WebGLRenderingContext {
    const context = this.gCanvas().getContext(this.props.contextType);
    if (!context) {
      const err = `No ${this.props.contextType} context`;
      alert(err);
      throw new Error(err);
    } else {
      switch (this.props.contextType) {
        case '2d':
        default:
          return context as CanvasRenderingContext2D
      }
    }
  }

  render() {
    const containerStyle = {
      width: '98%',
      maxWidth: '1400px',
      height: 'auto',
      border: 'solid grey 1px',
      display: 'grid',
      gridTemplateColumns: 'auto',
      gridTemplateRows: 'auto',
    };
    const canvasStyle = {
      width: '100%',
      height: 'auto',
      gridColumnStart: 1,
      gridRowStart: 1,
      zIndex: 1,
    };
    const overlayStyle = {
      width: '100%',
      height: '100%',
      gridColumnStart: 1,
      gridRowStart: 1,
      // backgroundColor: 'rgba(150,60,2,0.5)', // Un-comment to see me
      backgroundColor: 'rgba(255,255,255,0)',
      position: 'relative' as 'relative',
      cursor: this.decideOverlayCursor(this.props),
      zIndex: 2,
    };
    return (
      <div style={{
        display: 'flex',
        justifyContent: 'center',
      }}>
        <div style={containerStyle}>
          <div style={overlayStyle} ref={(input) => { this.overlay = input; }}/>
          <canvas
            id="whiteboard"
            width={this.props.width}
            height={this.props.height}
            style={canvasStyle}
            ref={(input) => { this.canvas = input; }}
          />
        </div>
      </div>
    );
  }
}

export { WhiteBoard };

export default connect<ValueProps,ActionProps,PublicProps,RootState>(mapStateToProps, mapDispatchToProps)(WhiteBoard);
