import * as React from 'react';
import { connect, ConnectedProps } from 'react-redux'
import { Tool } from '../../models';
import './index.scss';
import {RootState} from "../../state";
import { CHOOSE_TOOL } from "../../actions"

const mapState = (state: RootState) => ({
  toolList: state.tools,
  initialTool: state.currentSelection.toolId,
})

const mapDispatch = {
  onToolSelection: (toolId: string) => CHOOSE_TOOL.creator(toolId)
}

const connector = connect(mapState, mapDispatch)

type PropsFromRedux = ConnectedProps<typeof connector>

type Props = PropsFromRedux

interface State {
  toolSelected: string;
}

class ToolPicker extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { toolSelected: props.initialTool };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(toolID: string): () => any {
    return () => {
      this.setState({ toolSelected: toolID });
      this.props.onToolSelection(toolID);
    }
  }

  render() {
    return (
        <div id="toolPicker" style={{
          display: 'grid',
          gridTemplateColumns: '1fr 1fr',
        }}>
          {
            this.props.toolList.map((v:Tool, i:number) => {
              let cls = 'tool'
              if (v.id === this.state.toolSelected) {
                cls += ' selected'
              }
              return (
                <button key={i} className={cls} onClick={this.handleChange(v.id)}>
                  {v.icon}
                </button>
              )
            })
          }
        </div>
    )
  }
}

export default connector(ToolPicker)