import * as React from 'react';

interface Props {
  onUndo: () => any;
}

interface State {
  cmdIsDown: boolean;
}

const cmdKeys = ['Meta', 'Control'];

class UndoRedo extends React.Component<Props, State> {
  constructor(props: any) {
    super(props);

    this.state = {
      cmdIsDown: false,
    };
  }

  componentDidMount() {
    document.addEventListener('keydown', (event) => {
      if (cmdKeys.indexOf(event.key) > -1) {
        this.setState({ cmdIsDown: true });
      } else if (this.state.cmdIsDown && event.key === 'z') {
        this.props.onUndo();
      }
    });

    document.addEventListener('keyup', (event) => {
      if (cmdKeys.indexOf(event.key) > -1) {
        this.setState({ cmdIsDown: false });
      }
    });
  }

  render() {
    return (
      <div>
        <button style={{
          fontSize: '1.7em',
          width: '100%',
          backgroundColor: '#fcb900',
        }} onClick={this.props.onUndo}>
          ↩
        </button>
      </div>
    );
  }
}

export default UndoRedo;
