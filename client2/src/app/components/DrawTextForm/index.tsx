import * as React from 'react';

interface Props {
  onSubmit: (textToDraw: string) => any;
}

interface State {
  inputValue: string;
}

class DrawTextForm extends React.Component<Props, State> {
  inputRef: any;

  constructor(props: Props) {
    super(props);
    this.state = { inputValue: '' };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.obtainInputReference = this.obtainInputReference.bind(this);
  }

  componentDidMount() {
    this.inputRef.focus();
  }

  obtainInputReference(input: any) {
    this.inputRef = input;
  }

  handleChange(event: any) {
    this.setState({ inputValue: event.target.value });
  }

  handleSubmit(event: any) {
    event.preventDefault();
    if (this.isInputValueValid()) {
      this.props.onSubmit(this.state.inputValue);
    }
  }

  isInputValueValid() {
    return !!this.state.inputValue && this.state.inputValue.length > 0;
  }

  render() {
    return (
      <form className="form-inline" onSubmit={this.handleSubmit}>
        <div
          className="form-row align-items-center"
          style={{ width: '100%' }}
        >
          <div className="col-10">
            <div
              className="input-group"
              style={{ width: '100%' }}
            >
              <input
                style={{ width: '100%' }}
                id="chooseTextToDrawInput"
                type="text"
                ref={this.obtainInputReference}
                value={this.state.inputValue}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className="col-2">
            <button
              type="submit"
              className="btn btn-primary"
              disabled={!this.isInputValueValid()}
            >
              OK
            </button>
          </div>
        </div>
      </form>
    );
  }
}

export default DrawTextForm;
