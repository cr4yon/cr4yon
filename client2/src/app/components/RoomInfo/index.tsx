import * as React from 'react';
import { connect } from 'react-redux';
import { RootState, Room } from '../../state';
import {Action, AppThunkDispatch} from '../../actions';

const mapStateToProps = (state: RootState, ownProps: {}): Props => {
  if (!state.authentication.userName) {
    throw Error("no userName")
  }
  if (!state.room) {
    throw Error("no room")
  }
  return {
    ...ownProps,
    userName: state.authentication.userName,
    roomName: state.room && state.room.name,
    room: state.room,
  };
};

const mapDispatchToProps = (dispatch: AppThunkDispatch, ownProps: {}): any => {
  return {
    ...ownProps,
  };
};

interface Props {
  userName: string;
  roomName: string;
  room: Room;
}

interface MemberProps {
  userName: string;
  isConnected: boolean;
  isDrawing: boolean;
  drawingColor?: string;
}

class Member extends React.PureComponent<MemberProps> {
  constructor(props: MemberProps) {
    super(props);
  }

  render() {
    return this.props.isConnected ?
      (
        <strong style={{ color: this.props.drawingColor || '#000' }}>
          {this.props.userName}{this.props.isDrawing && ` (...)`}
        </strong>
      )
      :
      `${this.props.userName}`
    ;
  }
}

class RoomInfo extends React.PureComponent<Props> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    const members = this.props.room.members;
    return (
      <div style={{
        display: 'grid',
        gridTemplateColumns: 'repeat(3, 1fr)',
        gridAutoRows: 'minmax(30px, auto)',
      }}>
        <div style={{
          gridColumnStart: 1,
          gridColumnEnd: 2,
          gridRowStart: 1,
          gridRowEnd: 1,
        }}>
          <h1>🖍 {this.props.roomName}</h1>
        </div>
        <div style={{
          gridColumnStart: 2,
          gridColumnEnd: 4,
          gridRowStart: 1,
          gridRowEnd: 1,
        }}>
          <p>
            {members.map((m, i) => (
                <span key={i}>
                  {i > 0 && ', '}
                  <Member
                    userName={m.userName}
                    isConnected={m.isConnected}
                    isDrawing={m.isDrawing}
                    drawingColor={m.drawingColor}
                  />
                </span>
              ))
            } {members.length > 1 ? 'are' : 'is'} here.
          </p>
        </div>
      </div>
    );
  }
}

export { RoomInfo };
export default connect<Props, {}, {}, RootState>(mapStateToProps, mapDispatchToProps)(RoomInfo);
