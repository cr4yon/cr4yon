import * as React from 'react';

interface Props {
  embeddedFormUrl: string;
  width: number;
  height: number;
}

function GoogleFeedbackForm({embeddedFormUrl, width, height}: Props) {
    return (
      <div>
        <iframe
          style={{ width: '100%' }}
          src={embeddedFormUrl}
          width={`${width}`}
          height={`${height}`}
          frameBorder="0"
          marginHeight={0}
          marginWidth={0}
        >
          Loading feedback form...
        </iframe>
      </div>
    );
}

export default GoogleFeedbackForm;
