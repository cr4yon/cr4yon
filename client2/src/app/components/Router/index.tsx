import * as React from 'react';
import CSS from 'csstype';
import { Action } from '../../actions';
import { connect } from 'react-redux';
import { Authentication, Room, RootState } from '../../state';
import EntryForm from '../../components/EntryForm';
import GlobalError from '../../components/GlobalError';
import FeedbackPage from '../../pages/FeedbackPage';
import WhiteBoardPage from '../../pages/WhiteBoardPage';
import LogoWithText from '../../components/LogoWithText';

const mapStateToProps = (state: RootState, ownProps: {}): Props => {
  return {
    ...ownProps,
    authentication: state.authentication,
    room: state.room,
  };
};

const mapDispatchToProps = (dispatch: (action: Action) => any,
                            ownProps: {}): Partial<Props> => {
  return {
    ...ownProps,
  };
};

interface Props {
  authentication: Authentication | undefined;
  room: Room | undefined;
}

enum PAGE {
  ENTRY_FORM = 'ENTRY_FORM',
  WHITE_BOARD = 'WHITE_BOARD',
  FEEDBACK_FORM = 'FEEDBACK_FORM',
}

interface State {
  page: PAGE;
}

class Router extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = this.decideState(props);

    this.goToFeedbackPage = this.goToFeedbackPage.bind(this);
    this.goToWhiteBoardPage = this.goToWhiteBoardPage.bind(this);
  }

  decideState(props: Props) {
    if (this.isUserAuthenticated() && this.isRoomJoined()) {
      return {
        page: PAGE.WHITE_BOARD,
      };
    } else {
      return {
        page: PAGE.ENTRY_FORM,
      };
    }
  }

  componentWillReceiveProps(props: Props) {
    this.setState(this.decideState(props));
  }

  goToPage(page: PAGE) {
    this.setState({
      page,
    });
  }

  goToFeedbackPage() {
    this.goToPage(PAGE.FEEDBACK_FORM);
  }

  goToWhiteBoardPage() {
    this.goToPage(PAGE.WHITE_BOARD);
  }

  getPageToRender() {
    switch (this.state.page) {
      case PAGE.WHITE_BOARD:
        return (
          <WhiteBoardPage
            goToFeedbackPage={this.goToFeedbackPage}
          />
        );
      case PAGE.ENTRY_FORM:
        return (
          <div>
            <header
                style={{
                  marginBottom: '20px',
                  boxShadow: '0px 0px 3px #ccc',
                  backgroundColor: '#fff',
                }}
            >
              <div style={{
                height: '70px',
                maxWidth: '960px',
                margin: '0 auto',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
              }}>
                <div style={{}}>
                  <LogoWithText maxHeight={'30px'}/>
                </div>
              </div>
            </header>
            <div style={{}}>
              <div style={{
                maxWidth: '500px',
                margin: '60px auto 60px',
                display: 'flex',
                flexWrap: 'wrap',
              }}>
                <EntryForm/>
              </div>
            </div>
          </div>
        );
      case PAGE.FEEDBACK_FORM:
        return (
          <FeedbackPage
            goBack={this.goToWhiteBoardPage}
          />
        );
    }
  }

  isUserAuthenticated() {
    return this.props.authentication && this.props.authentication.userName;
  }

  isRoomJoined() {
    return this.props.room && this.props.room.name;
  }

  render() {
    const styles: CSS.Properties = {};
    const classes = '';
    return (
      <div className={classes} style={styles}>
        <GlobalError/>
        {
          this.getPageToRender()
        }
      </div>
    );
  }
}

export { Router };
export default connect(mapStateToProps, mapDispatchToProps)(Router);
