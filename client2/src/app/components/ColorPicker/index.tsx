import * as React from 'react';
import { connect } from 'react-redux';
import { RootState } from '../../state';
import { Action, CHOOSE_COLOR } from '../../actions';
// import { TwitterPicker as ExternalPicker } from 'react-color';
import { default as InternalPicker } from './InternalPicker';

const mapStateToProps = (state: RootState, ownProps: {}): PropsCurrentColor => {
  return {
    ...ownProps,
    currentColor: state.color,
  };
};

const mapDispatchToProps = (dispatch: (action: Action) => any,
                            ownProps: {}): PropsChooseColor => {
  return {
    ...ownProps,
    chooseColor: (color: string): void => dispatch(CHOOSE_COLOR.creator(color)),
  };
};

interface PropsChooseColor {
  chooseColor: (color: string) => any;
}

interface PropsCurrentColor {
  currentColor: string;
}

interface Props extends PropsChooseColor, PropsCurrentColor {}

interface State {
  inputValue: string;
}

class ColorPicker extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { inputValue: '' };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeComplete = this.handleChangeComplete.bind(this);
  }

  handleChange(event: Event): void {
    this.setState({ inputValue: (event.target as HTMLInputElement).value });
  }

  handleSubmit(event: Event): void {
    this.props.chooseColor(this.state.inputValue);
    event.preventDefault();
  }

  handleChangeComplete(color: any): void {
    this.props.chooseColor(color.hex);
  }

  isInputValueValid(): boolean {
    return this.state.inputValue.length === 7;
  }

  render(): JSX.Element {
    return (
      <InternalPicker
        color={this.props.currentColor}
        onChangeComplete={this.handleChangeComplete}
      />
    );
  }
}

export { ColorPicker };
export default connect(mapStateToProps, mapDispatchToProps)(ColorPicker);
