import { RootState } from '../state';

export function roomStateLoadedSubscriber(store: any) {
  return (state: RootState) => {
    const roomName = state.room!.name;
    // eslint-disable-next-line no-restricted-globals
    const params = new URLSearchParams(location.search);
    params.set('room', roomName);
    // eslint-disable-next-line no-restricted-globals
    window.history.replaceState({}, '', `${location.pathname}?${params}`);
  };
}
