import { Store } from 'redux'
import { roomStateLoaded$ } from './observables';
import { roomStateLoadedSubscriber } from './observable-subscribers';

function subscribeToStateObservables(store: Store): void {
  roomStateLoaded$(store).subscribe(roomStateLoadedSubscriber(store));
}

export default subscribeToStateObservables;
