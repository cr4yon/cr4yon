import {
  RootState,
  AuthenticationStatus,
  DrawingActionType,
  DrawingHistoricalEvent,
} from '../state';
import {
  Action,
  START_DRAWING_TEXT,
  HANDLE_LOCAL_DRAWING_EVENT,
  RECEIVE_DRAWING_EVENT,
  CHOOSE_COLOR,
  CHOOSE_LINE_WIDTH,
  JOIN_WHITEBOARD,
  DRAWING_STATE_RECEIVED,
  ROOM_STATE_RECEIVED,
  HANDLE_DISCONNECTION, StartDrawingTextAction,
  CHOOSE_TOOL,
} from '../actions';
import { DrawingEventType } from '../models';
import { Reducer } from "redux";


function removeLastUserEvent(events: DrawingHistoricalEvent[], userName: string): DrawingHistoricalEvent[] {
  let i = events.length - 1;
  let indexOfLastUserDrawingAction;

  while (!indexOfLastUserDrawingAction && i > -1) {
    const event = events[i];
    if (event.point.user === userName &&
        (event.type === DrawingEventType.DRAW_POINT || event.type === DrawingEventType.DRAW_TEXT)) {
      indexOfLastUserDrawingAction = i;
    }
    i = i - 1;
  }

  if (indexOfLastUserDrawingAction) {
    return events.slice(0, indexOfLastUserDrawingAction)
        .concat(events.slice(indexOfLastUserDrawingAction + 1));
  } else {
    return events;
  }
}

function buildHistoryWithoutUndone(events: DrawingHistoricalEvent[]): DrawingHistoricalEvent[] {
  const historySize = events.length;
  let i = 0;
  let indexOfUndoFound: number | undefined;
  let userNameOfUndo: string | undefined;
  while (i < historySize && !indexOfUndoFound) {
    const e = events[i];
    if (e.type === DrawingEventType.UNDO) {
      indexOfUndoFound = i;
      userNameOfUndo = e.point.user;
    } else {
      i = i + 1;
    }
  }

  if (indexOfUndoFound !== undefined && userNameOfUndo) {
    const intermediateHistory = removeLastUserEvent(events.slice(0, i), userNameOfUndo)
        .concat(
            events.slice(i + 1)
        );
    return buildHistoryWithoutUndone(intermediateHistory);
  } else {
    return events;
  }
}

const reducer: Reducer<RootState, Action> = (state: RootState | undefined, action: Action): RootState => {
  if (state === undefined) {
    throw Error("state is undefined")
  }
  switch (action.type) {
    case START_DRAWING_TEXT.type:
      // const a = action as StartDrawingTextAction // TODO Use action.payload<T>
      return {
        ...state,
        drawingState: {
          lastEventType: DrawingEventType.DRAW_TEXT,
          drawingActionType: DrawingActionType.TEXT,
          inProgress: true,
          details: {
            point: action.point,
            pagePosition: action.pagePosition,
          },
        },
      };
    case HANDLE_LOCAL_DRAWING_EVENT.type:
      switch (action.event.type) {
        case DrawingEventType.UNDO:
          return {
            ...state,
            drawingHistory: removeLastUserEvent(state.drawingHistory, action.event.point.user),
            drawingState: {
              ...state.drawingState,
              lastEventType: action.event.type,
              inProgress: action.event.type !== DrawingEventType.DRAW_TEXT,
            },
          };
        default:
          return {
            ...state,
            drawingHistory: state.drawingHistory.concat(action.event),
            drawingState: {
              ...state.drawingState,
              lastEventType: action.event.type,
              inProgress: action.event.type !== DrawingEventType.DRAW_TEXT,
            },
          };
      }
    case RECEIVE_DRAWING_EVENT.type:
      switch (action.event.type) {
        case DrawingEventType.UNDO:
          return {
            ...state,
            drawingHistory: removeLastUserEvent(state.drawingHistory, action.event.point.user),
          };
        default:
          return {
            ...state,
            drawingHistory: state.drawingHistory.concat(action.event),
          };
      }
    case CHOOSE_COLOR.type:
      return {
        ...state,
        color: action.color,
        palette: {
          ...state.palette,
          color: action.color,
        },
      };
    case CHOOSE_TOOL.type:
      return {
        ...state,
        currentSelection: {
          ...state.currentSelection,
          toolId: action.toolId,
        },
      };
    case CHOOSE_LINE_WIDTH.type:
      return {
        ...state,
        palette: {
          ...state.palette,
          lineWidth: action.lineWidth,
        },
      };
    case ROOM_STATE_RECEIVED.type:
      return {
        ...state,
        room: action.room,
      };
    case DRAWING_STATE_RECEIVED.type:
      return {
        ...state,
        room: action.drawingState.room,
        drawingHistory: buildHistoryWithoutUndone(action.drawingState.drawingEvents),
      };
    case HANDLE_DISCONNECTION.type:
      return {
        ...state,
        authentication: { status: AuthenticationStatus.FAILED },
        error: `You are disconnected ⚡️. You can still draw but not collaboratively. 
Please refresh ♻️ and join again.`,
      };
    case JOIN_WHITEBOARD.type:
      switch (action.result) {
        case 'DECLINED':
          return {
            ...state,
            authentication: { status: AuthenticationStatus.FAILED },
            error: action.message,
          };
        case 'ACCEPTED':
          return {
            ...state,
            room: {
              name: action.roomName,
              members: [],
            },
            authentication: {
              status: AuthenticationStatus.COMPLETE,
              userName: action.userName,
            },
          };
      }
      break
    default:
      return state;
  }
  return state;
};

export default reducer;
