import { Store } from 'redux';

import {
  DRAWING_STATE_RECEIVED,
  ROOM_STATE_RECEIVED,
  RECEIVE_DRAWING_EVENT,
  HANDLE_DISCONNECTION,
} from '../actions';

interface SocketSubscribers {
  onDrawingEvent: (msg: any) => any;
  onDrawingStateReceived: (state: any) => any;
  onRoomStateReceived: (state: any) => any;
  onDisconnection: () => any;
}

function forStore(store: Store): SocketSubscribers {
  return {
    onDrawingEvent: (event: any) => {
      store.dispatch(RECEIVE_DRAWING_EVENT.creator(event));
    },

    onDrawingStateReceived: (state: any) => {
      store.dispatch(DRAWING_STATE_RECEIVED.creator(state));
    },

    onRoomStateReceived: (state: any) => {
      store.dispatch(ROOM_STATE_RECEIVED.creator(state));
    },

    onDisconnection: () => {
      store.dispatch(HANDLE_DISCONNECTION.creator());
    },
  };
}

export default {
  forStore,
};
