import {
  DrawPointEvent,
  StopDrawingEvent,
  DrawingEvent,
  StartDrawingTextEvent,
  DrawTextEvent,
  ContinueDrawingTextEvent,
  Position,
} from '../../models';
import SimpleEventPublisher from '../SimpleEventPublisher';

// TODO should handle multiple text being written
export default class CanvasUserIntentTextMouseListener {
  canvas: any;
  drawingEventHandler: (drawingEvent: DrawingEvent) => any;
  internalEventPublisher: SimpleEventPublisher;
  isDrawingText: boolean;
  currentTextValue: string | null;
  startDrawTextPosition: Position | null;
  observers: MutationObserver[];

  constructor(canvas: any, drawingEventHandler: (drawingEvent: DrawingEvent) => any) {
    this.canvas = canvas;
    this.drawingEventHandler = drawingEventHandler;
    this.internalEventPublisher = new SimpleEventPublisher();
    this.isDrawingText = false;
    this.currentTextValue = null;
    this.startDrawTextPosition = null;
    this.observers = [];

    this.onMouseDownHandler = this.onMouseDownHandler.bind(this)
  }

  start() {
    this.setUpEventListeners();
    this.setUpInternalEventSubscribers();
  }

  stop() {
    this.removeEventListeners();
  }

  setUpEventListeners() {
    this.canvas.addEventListener('mousedown', this.onMouseDownHandler);
    this.observeCanvas();
  }

  observeCanvas() {
    // Options for the observer (which mutations to observe)
    const config = { attributes: false, childList: true, subtree: false };
    // Callback function to execute when mutations are observed
    const callback: MutationCallback = (mutationsList: MutationRecord[], observer: MutationObserver) => {
      // Use traditional 'for loops' for IE 11
      for(let mutation of mutationsList) {
        console.log("mutation", mutation);
        if (mutation.type === 'childList') {
          if (mutation.addedNodes.length > 0) {
            mutation.addedNodes.forEach(n => {
              const el = (n as HTMLElement);
              if (el.id === 'drawTextInputContainer') {
                this.setupInputListener(el.getElementsByTagName('input')[0]);
              }
            })
          }
        }
      }
    };
    // Create an observer instance linked to the callback function
    const observer = new MutationObserver(callback);
    // Start observing the target node for configured mutations
    observer.observe(this.canvas, config);

    this.observers.push(observer);
  }

  setupInputListener(input: HTMLInputElement) {
    input.oninput = (e: Event) => {
      const newVal = (input as HTMLInputElement).value;
      console.info('newVal', newVal);
      this.currentTextValue = newVal;
      if (this.startDrawTextPosition === null) {
        console.error("No position for start draw text, cannot continue");
        return
      }
      this.drawingEventHandler(
          new ContinueDrawingTextEvent(
              { position: this.startDrawTextPosition },
              newVal,
          )
      );
    }
  }

  removeEventListeners() {
    this.canvas.removeEventListener('mousedown', this.onMouseDownHandler);
    this.observers.forEach(o => o.disconnect());
  }

  setUpInternalEventSubscribers() {
    // this.internalEventPublisher.addSubscriber('doubleClick', (event: MouseEvent) => {
    //   this.onDoubleClickHandler(event);
    // });
  }

  onMouseDownHandler(event: MouseEvent) {
    if (this.canvas != event.target) return;

    if (this.isDrawingText) {
      this.stopDrawText(event);
      this.isDrawingText = false;
    } else {
      this.startDrawText(event);
      this.isDrawingText = true;
    }
  }

  startDrawText(event: MouseEvent) {
    // TODO handle input hover canvas here
    this.startDrawTextPosition = this.normalisePosition(event);
    this.drawingEventHandler(
      new StartDrawingTextEvent(
        { position: this.startDrawTextPosition },
          { x: event.pageX, y: event.pageY },
      )
    );
  }

  stopDrawText(event: MouseEvent) {
    this.drawingEventHandler(
        new StopDrawingEvent(
            { position: this.normalisePosition(event) },
        )
    );
    this.startDrawTextPosition = null;
  }

  normalisePosition(event: MouseEvent) {
    return {
      x: (event.pageX - this.canvas.offsetLeft) / this.canvas.clientWidth,
      y: (event.pageY - this.canvas.offsetTop) / this.canvas.clientHeight,
    };
  }
}
