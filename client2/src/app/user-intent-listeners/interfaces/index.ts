export interface UserIntentListener {
  start: () => any;
  stop: () => void;
}
