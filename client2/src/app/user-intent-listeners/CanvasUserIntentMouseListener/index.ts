import {
  DrawPointEvent,
  StopDrawingEvent,
  DrawingEvent,
  StartDrawingTextEvent,
} from '../../models';
import SimpleEventPublisher from '../SimpleEventPublisher';
import {distance} from "../../algebra/distance";

const DOUBLE_CLICK_MAX_INTERVAL = 500;

export default class CanvasUserIntentMouseListener {
  canvas: any;
  drawingEventHandler: (drawingEvent: DrawingEvent) => any;
  lastTapTime: number | undefined;
  internalEventPublisher: SimpleEventPublisher;
  mouseIsDown: boolean;
  minimumDistanceBeforeNextPoint: number;

  constructor(canvas: any,
              drawingEventHandler: (drawingEvent: DrawingEvent) => any,
              minimumDistanceBeforeNextPoint: number) {
    this.canvas = canvas;
    this.drawingEventHandler = drawingEventHandler;
    this.internalEventPublisher = new SimpleEventPublisher();
    this.mouseIsDown = false;
    this.minimumDistanceBeforeNextPoint = minimumDistanceBeforeNextPoint;

    this.onMouseDownHandler = this.onMouseDownHandler.bind(this)
    this.onMouseUpHandler = this.onMouseUpHandler.bind(this)
  }

  start() {
    this.setUpEventListeners();
    this.setUpInternalEventSubscribers();
  }

  stop() {
    this.removeEventListeners();
  }

  setUpEventListeners() {
    this.canvas.addEventListener('mousedown', this.onMouseDownHandler);
    this.canvas.addEventListener('mouseup', this.onMouseUpHandler);
  }

  removeEventListeners() {
    this.canvas.removeEventListener('mousedown', this.onMouseDownHandler)
    this.canvas.removeEventListener('mouseup', this.onMouseUpHandler)
  }

  setUpInternalEventSubscribers() {
    this.internalEventPublisher.addSubscriber('doubleClick', (event: MouseEvent) => {
      this.onDoubleClickHandler(event);
    });
  }

  onDoubleClickHandler(event: MouseEvent) {
    this.drawingEventHandler(
      new StartDrawingTextEvent(
        { position: this.normalisePosition(event) },
        { x: event.pageX, y: event.pageY }
      )
    );
  }

  onMouseUpHandler(event: MouseEvent) {
    this.mouseIsDown = false;
    const currentTime = new Date().getTime();
    if (this.lastTapTime === undefined) {
      this.lastTapTime = currentTime;
      return
    }
    const tapLength = currentTime - this.lastTapTime;
    if (tapLength < DOUBLE_CLICK_MAX_INTERVAL && tapLength > 0) {
      event.preventDefault();
      this.internalEventPublisher.publish('doubleClick', event);
    }
    this.lastTapTime = currentTime;
  }

  formDrawPointEvent(event: MouseEvent) {
    return new DrawPointEvent({
      position: this.normalisePosition(event),
    });
  }

  normalisePosition(event: MouseEvent) {
    return {
      x: (event.pageX - this.canvas.offsetLeft) / this.canvas.clientWidth,
      y: (event.pageY - this.canvas.offsetTop) / this.canvas.clientHeight,
    };
  }

  onMouseDownHandler(event: MouseEvent) {
    this.mouseIsDown = true;
    let firstPointDrawn = false;
    let lastPointEvent: DrawPointEvent | null = null;
    const firstEvent = event;
    const eventPublisher = new SimpleEventPublisher();

    const onMouseMoveHandler = (event: any) => {
      if (!firstPointDrawn) {
        this.drawingEventHandler(
          this.formDrawPointEvent(firstEvent)
        );
        firstPointDrawn = true;
        eventPublisher.publish('drawingStarted', firstEvent);
      }

      const drawPointEvent = this.formDrawPointEvent(event)
      if (lastPointEvent === null) {
        lastPointEvent = drawPointEvent
      }

      const d = distance(drawPointEvent.point.position, lastPointEvent.point.position)
      if (d < this.minimumDistanceBeforeNextPoint) {
        return
      }

      this.drawingEventHandler(drawPointEvent);
      lastPointEvent = drawPointEvent;
    };

    const cancelDrawingMouseUpHandler = (event: MouseEvent) => {
      this.canvas.removeEventListener('mousemove', onMouseMoveHandler);
    };
    this.canvas.addEventListener('mouseup', cancelDrawingMouseUpHandler);

    if (this.mouseIsDown) {
      this.canvas.addEventListener('mousemove', onMouseMoveHandler);
    }

    this.internalEventPublisher.addSubscriber(
      'doubleClick',
      () => this.canvas.removeEventListener('mousemove', onMouseMoveHandler)
    );

    const finishDrawingMouseUpHandler = (event: MouseEvent) => {
      this.canvas.removeEventListener('mousemove', onMouseMoveHandler);
      this.drawingEventHandler(
        new StopDrawingEvent({
          position: this.normalisePosition(event),
        })
      );
      eventPublisher.publish('mouseUpForDrawingApplied');
    };

    eventPublisher.addSubscriber(
      'mouseUpForDrawingApplied',
      () => this.canvas.removeEventListener('mouseup', finishDrawingMouseUpHandler)
    );

    // this.canvas.addEventListener('mouseup', mouseUpHandler);
    eventPublisher.addSubscriber(
      'drawingStarted',
      () => {
        this.canvas.removeEventListener('mousemove', cancelDrawingMouseUpHandler);
        this.canvas.addEventListener('mouseup', finishDrawingMouseUpHandler);
      }
    );
  }
}
