import {
  DrawPointEvent,
  StopDrawingEvent,
  DrawingEvent,
  StartDrawingTextEvent,
} from '../../models';
import SimpleEventPublisher from '../SimpleEventPublisher';

export default class CanvasUserIntentTextTouchListener {
  canvas: any;
  drawingEventHandler: (drawingEvent: DrawingEvent) => any;
  internalEventPublisher: SimpleEventPublisher;
  isDrawingText: boolean;

  constructor(canvas: any, drawingEventHandler: (drawingEvent: DrawingEvent) => any) {
    this.canvas = canvas;
    this.drawingEventHandler = drawingEventHandler;
    this.internalEventPublisher = new SimpleEventPublisher();
    this.isDrawingText = false;

    this.onTouchStartHandler = this.onTouchStartHandler.bind(this)
  }

  start() {
    this.setUpEventListeners();
    this.setUpInternalEventSubscribers();
  }

  stop() {
    this.removeEventListeners();
  }

  setUpEventListeners() {
    this.canvas.addEventListener('touchstart', this.onTouchStartHandler);
  }

  removeEventListeners() {
    this.canvas.removeEventListener('touchstart', this.onTouchStartHandler)
  }

  setUpInternalEventSubscribers() {
    // this.internalEventPublisher.addSubscriber('doubleClick', (event: MouseEvent) => {
    //   this.onDoubleClickHandler(event);
    // });
  }

  onTouchStartHandler(event: TouchEvent) {
    if (this.isDrawingText) {
      this.stopDrawText(event);
      this.isDrawingText = false;
    } else {
      this.startDrawText(event);
      this.isDrawingText = true;
    }
  }

  startDrawText(event: TouchEvent) {
    const touches = event.touches.length > 0 ? event.touches : event.changedTouches;
    const firstTouch = touches[0];
    this.drawingEventHandler(
        new StartDrawingTextEvent(
            { position: this.normalisePosition(firstTouch) },
            { x: firstTouch.pageX, y: firstTouch.pageY }
        )
    );
  }

  stopDrawText(event: TouchEvent) {
    const touches = event.touches.length > 0 ? event.touches : event.changedTouches;
    const firstTouch = touches[0];
    this.drawingEventHandler(
        new StopDrawingEvent(
            { position: this.normalisePosition(firstTouch) },
        )
    );
  }

  normalisePosition(event: Touch) {
    return {
      x: (event.pageX - this.canvas.offsetLeft) / this.canvas.clientWidth,
      y: (event.pageY - this.canvas.offsetTop) / this.canvas.clientHeight,
    };
  }
}
