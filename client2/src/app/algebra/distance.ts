import {Position} from "../models";

export function distance(p1: Position, p2: Position): number {
  return Math.abs(p1.x-p2.x) + Math.abs(p1.y-p2.y)
}