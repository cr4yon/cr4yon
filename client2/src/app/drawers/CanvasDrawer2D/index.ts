import { Point, Position, DrawPointStrategy } from '../../models';
import { Drawer, DrawTextCommand } from '../interfaces';


function drawCircle(context: CanvasRenderingContext2D, positionCenter: Position, radius: number, color?: string): void {
  if (color) {
    context.strokeStyle = color;
  }
  context.beginPath();
  context.arc(positionCenter.x, positionCenter.y, radius, 0, 2 * Math.PI);
  context.stroke();
}

export default class Index implements Drawer {
  canvas: HTMLCanvasElement;
  overlay: HTMLDivElement;
  context: CanvasRenderingContext2D;
  drawPointStrategy: DrawPointStrategy;
  lastPoint: Point | null;
  textPosition: Position | null;

  constructor(canvas: HTMLCanvasElement, overlay: HTMLDivElement, context: CanvasRenderingContext2D, drawPointStrategy: DrawPointStrategy) {
    this.canvas = canvas;
    this.overlay = overlay;
    this.context = context;
    this.drawPointStrategy = drawPointStrategy;
    this.lastPoint = null;
    this.textPosition = null;
  }

  clean(): void {
    this.lastPoint = null;
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }

  fillCanvasBackground(fillingColor: string): void {
    this.context.fillStyle = fillingColor;
    this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
  }

  stopDrawing(): void {
    this.lastPoint = null;
    this.stopDrawingText();
  }

  absolutePosition(position: Position): Position {
    return {
      x: position.x * this.canvas.width,
      y: position.y * this.canvas.height,
    };
  }

  absolutePositionForOverlay(position: Position): Position {
    return {
      x: position.x * this.overlay.clientWidth,
      y: position.y * this.canvas.clientHeight,
    };
  }

  startDrawText(position: Position): void {
    this.stopDrawingText();
    this.textPosition = position;

    const newContainer = document.createElement("div");
    newContainer.setAttribute('id', 'drawTextInputContainer')
    const absPos = this.absolutePositionForOverlay(position);
    newContainer.style.position = "absolute";
    newContainer.style.left = `${absPos.x}px`;
    newContainer.style.top = `${absPos.y}px`;
    newContainer.style.border = "solid black 2px";
    const input = document.createElement("input");
    input.setAttribute('id', 'drawTextInput')
    newContainer.appendChild(input);
    this.overlay.appendChild(newContainer);
    setTimeout(() => {
      const i = document.getElementById('drawTextInput');
      if (i !== null) {
        (i as HTMLInputElement).focus();
      }
    }, 20);
  }

  continueDrawingText(newValue: string): void {
    const i = document.getElementById('drawTextInput');
    if (i !== null) {
      (i as HTMLInputElement).focus();
      i.setAttribute('value', newValue)
    }
  }

  // TODO Draw or not should come from event, shouldnot decide here
  stopDrawingText(): void {
    const previousInput = document.getElementById('drawTextInput');
    if (previousInput != null) {
      const parent = previousInput.parentElement;
      const val = (previousInput as HTMLInputElement).value;
      if (!val || val == '') {
        console.info('text to draw is empty, ignoring');
      } else if (this.textPosition !== null && parent !== null) {
        const drawTextCommand = {
          position: this.textPosition,
          color: '#000000', // fix
          lineWidth: 2, // fix
          textValue: val,
        };
        this.drawText(drawTextCommand);
      } else {
        console.error("could not draw text value ", val)
      }
      previousInput.remove();
    }
    const previousContainer = document.getElementById('drawTextInputContainer');
    if (previousContainer != null) {
      previousContainer.remove();
    }

    this.textPosition = null;
  }

  drawText(cmd: DrawTextCommand): void {
    const absolutePosition = this.absolutePosition(cmd.position);
    this.context.font = '16px Arial';
    this.context.fillStyle = cmd.color || 'black';
    this.context.fillText(cmd.textValue, absolutePosition.x, absolutePosition.y);
  }

  drawPoint(point: Point): void {
    if (!this.lastPoint) {
      this.lastPoint = point;
    } else {
      switch (this.drawPointStrategy) {
        case DrawPointStrategy.DISCRETE:
          drawCircle(
            this.context,
            this.absolutePosition(point.position),
            5,
            point.color
          );
          return;
        case DrawPointStrategy.LINE:
          this.drawLine(
            this.absolutePosition(this.lastPoint.position),
            this.absolutePosition(point.position),
            point.color,
            point.lineWidth
          );
          this.lastPoint = point;
          return;
        default:
          throw new Error('Unknow draw point strategy' + this.drawPointStrategy);
      }
    }
  }

  drawLine(positionFrom: Position, positionTo: Position, color?: string, lineWidth?: number): void {
    this.context.beginPath();
    if (color) {
      this.context.strokeStyle = color;
    }
    if (lineWidth) {
      this.context.lineWidth = lineWidth;
    }
    this.context.moveTo(positionFrom.x, positionFrom.y);
    this.context.lineTo(positionTo.x, positionTo.y);
    this.context.stroke();
    this.context.closePath();
  }
}
