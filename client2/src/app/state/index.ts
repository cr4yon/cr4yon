import { Point, DrawingEventType, Tool } from '../models';

export enum AuthenticationStatus {
  PENDING = 'pending',
  FAILED = 'failed',
  COMPLETE = 'complete',
}

export enum DrawingActionType {
  POINT = 'POINT',
  TEXT = 'TEXT',
}

export interface DrawingHistoricalEvent {
  type: DrawingEventType;
  point: Point;
  details?: any;
}

export interface Authentication {
  status: AuthenticationStatus;
  userName?: string;
}

export interface Room {
  name: string;
  members: any[];
}

export interface Palette {
  color: string;
  lineWidth: number;
}

export interface CurrentSelection {
  toolId: string;
}

export interface RootState {
  socket: any; // improve
  room?: Room;
  drawingHistory: DrawingHistoricalEvent[];
  drawingState: {
    drawingActionType: DrawingActionType;
    lastEventType: DrawingEventType;
    inProgress: boolean;
    details?: any;
  };
  color: string;
  palette: Palette;
  error?: string;
  authentication: Authentication;
  tools: Tool[];
  currentSelection: CurrentSelection;
}
