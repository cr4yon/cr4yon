export interface Tool {
  id: string;
  displayName: string;
  icon: string;
  options: string[];
  cursor: string,
}

export enum PointType {
  START = 'start',
  STOP = 'stop',
}

export interface Point {
  position: Position;
  type?: PointType;
  color?: string;
  lineWidth?: number;
  user?: string;
}

export interface Position {
  x: number;
  y: number;
}

export enum DrawPointStrategy {
  LINE = 'line',
  DISCRETE = 'discrete',
}

export enum DrawingEventType {
  DRAW_POINT = 'event.drawPoint',
  STOP_DRAWING = 'event.stopDrawing',
  START_DRAWING_TEXT = 'event.startDrawingText',
  CONTINUE_DRAWING_TEXT = 'event.continueDrawingText',
  DRAW_TEXT = 'event.drawText',
  UNDO = 'event.undo',
}

export class DrawingEvent {
  point: Point;
  type: DrawingEventType;

  constructor(type: DrawingEventType, point: Point) {
    this.type = type;
    this.point = point;
  }
}

export class StartDrawingTextEvent extends DrawingEvent {
  absolutePagePosition: Position;
  constructor(point: Point, absolutePagePosition: Position) {
    super(DrawingEventType.START_DRAWING_TEXT, point);
    this.absolutePagePosition = absolutePagePosition;
  }
}

export class ContinueDrawingTextEvent extends DrawingEvent {
  newValue: string;
  constructor(point: Point, newValue: string) {
    super(DrawingEventType.CONTINUE_DRAWING_TEXT, point);
    this.newValue = newValue;
  }
}

export class DrawTextEvent extends DrawingEvent {
  text: string;

  constructor(point: Point, text: string) {
    super(DrawingEventType.DRAW_TEXT, point);
    this.text = text;
  }
}

export class DrawPointEvent extends DrawingEvent {
  constructor(point: Point) {
    super(DrawingEventType.DRAW_POINT, point);
  }
}

export class StopDrawingEvent extends DrawingEvent {
  constructor(point: Point) {
    super(DrawingEventType.STOP_DRAWING, point);
  }
}
