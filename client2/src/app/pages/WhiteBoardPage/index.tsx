import * as React from 'react';
import { connect } from 'react-redux';
import ColorPicker from '../../components/ColorPicker';
import LineWidthPicker from '../../components/LineWidthPicker';
import WhiteBoard from '../../components/WhiteBoard';
import RoomInfo from '../../components/RoomInfo';
import DrawTextForm from '../../components/DrawTextForm';
import Tips from '../../components/Tips';
import ToolPicker from '../../components/ToolPicker';
import UndoRedo from '../../components/UndoRedo';
import { RootState } from '../../state';
import {
    Action, AppThunkDispatch,
    HANDLE_LOCAL_DRAWING_EVENT,
} from '../../actions';
import { Point, Position, DrawingEventType } from '../../models';

interface PublicProps {
    goToFeedbackPage: () => void;
}

const isDrawingText = (state: RootState) => (
  state.drawingState.lastEventType === DrawingEventType.DRAW_TEXT && state.drawingState.inProgress
);

const mapStateToProps = (state: RootState, ownProps: {}): ValueProps => {
  return {
    ...ownProps,
    showDrawingTextForm: isDrawingText(state),
    textToDrawPoint: isDrawingText(state) && state.drawingState.details.point,
    pagePositionOfTextToDraw: isDrawingText(state) && state.drawingState.details.pagePosition,
  };
};

const mapDispatchToProps = (dispatch: AppThunkDispatch,
                            ownProps: PublicProps): ActionProps => {
  return {
    ...ownProps,
    onTextToDrawSubmit: (point: Point, text: string) => (
      dispatch(
        HANDLE_LOCAL_DRAWING_EVENT.creator(DrawingEventType.DRAW_TEXT, point, { textValue: text })
      )
    ),
    onUndo: () => (
      dispatch(
        HANDLE_LOCAL_DRAWING_EVENT.creator(
          DrawingEventType.UNDO,
          { position: { x: 0, y: 0 } }, // hack
          {}
        )
      )
    ),
  };
};

interface ValueProps {
    showDrawingTextForm: boolean;
    textToDrawPoint?: Point;
    pagePositionOfTextToDraw?: Position;
}

interface ActionProps {
    onTextToDrawSubmit: (point: Point, text: string) => void;
    onUndo: () => void;
    goToFeedbackPage: Function;
}

interface Props extends ValueProps, ActionProps {}

interface State {
  showTips: boolean;
}

class WhiteBoardPage extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      showTips: false,
    };

    this.onGoToFeedbackPageClick = this.onGoToFeedbackPageClick.bind(this);
    this.toggleTips = this.toggleTips.bind(this);
  }

  onGoToFeedbackPageClick(event: any) {
    event.preventDefault();
    this.props.goToFeedbackPage();
  }

  toggleTips(event: any) {
    event.preventDefault();
    this.setState({ showTips: !this.state.showTips });
  }

  render() {
    const sideBoxStyle = {
      padding: '10px 8px',
      borderRadius: '4px',
      backgroundColor: 'rgba(255, 255, 255, 0.2)',
    };
    return (
      <div className="wrapper"
           style={{
             backgroundColor: '#444',
             padding: '8px',
             display: 'grid',
             gridTemplateColumns: '1fr',
             gridTemplateRows: '1fr 20fr',
             rowGap: '6px',
             gridTemplateAreas: `
             "nav" 
             "main" `
           }}>
        <nav style={{
          gridArea: 'nav',
          color: '#fff',
        }}>
          <div style={{
            display: 'grid',
            gridTemplateColumns: 'repeat(3, 1fr)',
          }}>
            <div style={{
              gridColumnStart: 1,
              gridColumnEnd: 3,
              gridRowStart: 1,
              gridRowEnd: 1,
            }}>
              <RoomInfo/>
            </div>
            <div style={{
              gridColumnStart: 3,
              gridColumnEnd: 4,
              gridRowStart: 1,
              gridRowEnd: 1,
            }}>
              <div style={{textAlign: 'right'}}>
                <button onClick={this.onGoToFeedbackPageClick}>
                  Feedback
                </button>
                <button onClick={this.toggleTips} className="m-l-1">
                  Tips
                </button>
              </div>
              { this.state.showTips && <div style={{fontSize: '0.7em'}}><Tips/></div> }
            </div>
          </div>
        </nav>
        <main
          style={{
            gridArea: 'main',
            margin: '0',
          }}
        >
          <div style={{
            display: 'grid',
            gridTemplateColumns: 'minmax(60px,80px) 20fr minmax(60px,80px)',
            columnGap: '6px',
          }}>
            <div style={{
              gridColumnStart: 1,
              gridColumnEnd: 2,
              gridRowStart: 1,
              gridRowEnd: 1,
              padding: '4px',
            }}>
              <div style={sideBoxStyle} className='m-b-2'>
                <ToolPicker/>
              </div>
              <div style={sideBoxStyle}>
                <div style={{
                  display: 'grid',
                  gridTemplateColumns: '1fr',
                  gridGap: '6px',
                }}>
                  <div>
                    <LineWidthPicker/>
                  </div>
                  <div>
                    <ColorPicker/>
                  </div>
                </div>
              </div>
            </div>
            <div style={{
              gridColumnStart: 2,
              gridColumnEnd: 3,
              gridRowStart: 1,
              gridRowEnd: 1,
            }}>
              <WhiteBoard
                contextType={'2d'}
                width={800}
                height={500}
              />
            </div>
            <div style={{
              gridColumnStart: 3,
              gridColumnEnd: 4,
              gridRowStart: 1,
              gridRowEnd: 1,
            }}>
              <div style={sideBoxStyle}>
                <div>
                  <UndoRedo onUndo={this.props.onUndo}/>
                </div>
              </div>
            </div>
          </div>
          </main>
      </div>
    );
  }
}

export { WhiteBoardPage };
export default connect<ValueProps, ActionProps, PublicProps, RootState>(mapStateToProps, mapDispatchToProps)(WhiteBoardPage);
