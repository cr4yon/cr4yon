import * as React from 'react';
import FeedbackForm from '../../components/FeedbackForm';

interface Props {
  goBack?: Function;
  goBackRender?: Function;
}

interface State {
  goBackClicked: boolean;
}

class FeedbackPage extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = { goBackClicked: false };

    this.onGoBackClick = this.onGoBackClick.bind(this);
  }

  onGoBackClick(e: any) {
    e.preventDefault();
    this.setState({ goBackClicked: true });
    if (this.props.goBack) this.props.goBack();
  }

  render() {
    return this.state.goBackClicked && this.props.goBackRender ?
      this.props.goBackRender() :
      (
      <div>
        <header className="container-fluid">
          <div
            className="row h-100 align-items-center"
            style={{
              borderBottom: 'solid 1px #eee',
            }}
          >
            <div className="col-4">
              <button
                onClick={this.onGoBackClick}
                className="btn btn-link"
                style={{
                  fontSize: '1.5em',
                }}
              >
                &larr;<span className="d-none d-sm-inline"> Back</span>
              </button>
            </div>
            <div className="col-4 text-center">
              <h4 className="my-auto">
                <span className="align-middle">👂</span>
              </h4>
            </div>
            <div className="d-none d-sm-inline-block col-4">
                <a
                  href="https://github.com/adrienDog/crayon-issues/issues"
                  target="_blank"
                  className="btn btn-danger btn-block"
                >
                  Report an issue ⚡️
                </a>
              </div>
          </div>
        </header>
        <main className="my-2 container">
          <p>
            If you have no issue to report,
            you can leave constructive feedback in the form below 🙌.
          </p>
          <div className="text-center">
            <FeedbackForm width={750} height={800}/>
          </div>
        </main>
        <footer
          style={{
            position: 'fixed',
            bottom: '0',
            left: '0',
            width: '100%',
            boxShadow: '0px 0px 3px #ccc',
            backgroundColor: '#fff',
          }}
        >
          <div className="container-fluid">
            <div className="row py-1 justify-content-end">
              <div className="d-sm-none col-12">
                <a
                  href="https://github.com/adrienDog/crayon-issues/issues"
                  target="_blank"
                  className="btn btn-danger btn-block"
                >
                  Report an issue ⚡️
                </a>
              </div>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}

export default FeedbackPage;
