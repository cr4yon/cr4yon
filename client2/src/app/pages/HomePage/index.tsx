import * as React from 'react';
import image from './example.png';
import LogoWithText from '../../components/LogoWithText';

interface Props {
  onGetStartedRender: Function;
  onGiveFeedbackRender: Function;
  maxHeight?: string;
}

interface State {
  getStartedClicked: boolean;
  giveFeedbackClicked: boolean;
}

export default class HomePage extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      getStartedClicked: false,
      giveFeedbackClicked: false,
    };

    this.onGetStartedClick = this.onGetStartedClick.bind(this);
    this.onGiveFeedbackClick = this.onGiveFeedbackClick.bind(this);
  }

  onGetStartedClick(e: any) {
    e.preventDefault();
    this.setState({ getStartedClicked: true });
  }

  onGiveFeedbackClick(e: any) {
    e.preventDefault();
    this.setState({ giveFeedbackClicked: true });
  }

  render() {
    if (this.state.getStartedClicked) {
      return this.props.onGetStartedRender();
    } else if (this.state.giveFeedbackClicked) {
      return this.props.onGiveFeedbackRender();
    } else {
      return (
        <div className="wrapper" style={{
          // display: 'grid',
          // gridTemplateColumns: '1fr',
          // gridTemplateRows: '1fr 15fr 5fr',
          // gridRowGap: '4px',
          // gridTemplateAreas: `
          //    "header"
          //    "main"
          //    "footer"`,
        }}>
          <header
            style={{
              marginBottom: '20px',
              boxShadow: '0px 0px 3px #ccc',
              backgroundColor: '#fff',
            }}
          >
            <div style={{
              height: '70px',
              maxWidth: '960px',
              margin: '0 auto',
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
            }}>
              <div style={{}}>
                <LogoWithText maxHeight={'30px'}/>
              </div>
            </div>
          </header>

          <main style={{
            // gridArea: 'main',
          }}>
            <section style={{}}>
              <div style={{
                maxWidth: '960px',
                margin: '60px auto 60px',
                display: 'flex',
                flexWrap: 'wrap',
              }}>
                <div style={{minWidth: '260px', maxWidth: '500px'}}>
                  <img
                      src={image}
                      style={{
                        maxHeight: this.props.maxHeight || '400px',
                        maxWidth: '100%',
                      }}
                  />
                </div>
                <div style={{minWidth: '260px', maxWidth: '500px'}}>
                  <h1>
                    Where minds come together
                  </h1>
                  <p>
                    When you need a decision to emerge naturally from white-boarding.
                  </p>
                  <div>
                    <button onClick={this.onGetStartedClick}>
                      Get started
                    </button>
                  </div>
                </div>
              </div>
            </section>
            <section
              style={{
                // gridArea: 'footer',
                backgroundColor: 'rgb(56, 255, 176)',
                padding: '40px 0'
              }}
            >
              <div style={{
                maxWidth: '960px',
                margin: '40px auto 40px',
              }}>
                <div style={{}}>
                  <h2>
                    We're in alpha
                  </h2>
                  <p>
                    Lucky you! You get to try out our collaborative white board and give feedback
                  </p>
                  <div>
                    <button className="btn btn-primary" onClick={this.onGiveFeedbackClick}>
                      Give feedback
                    </button>
                  </div>
                </div>
              </div>
            </section>
          </main>
        </div>
      );
    }
  }
}
