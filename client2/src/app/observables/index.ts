import { observableFromStore } from 'redux-rx';
import { RootState } from '../state';

export function roomStateLoaded$(store: any) {
  const state$ = observableFromStore(store);
  return state$
    .distinctUntilChanged((state: RootState) => (
      state.room
    ))
    .filter((state: RootState) => (
      state.room && state.room.name
    ));
}
