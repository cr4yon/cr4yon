const { Router } = require('express');
const analyticsRoute = require('./analytics.route');
const client = require('prom-client');

const router = Router();

router.get('/', (req, res) => {
  res.render('index', { title: '🖍 Cr4yon API' });
});

router.get('/metrics', (req, res) => {
  res.set('Content-Type', client.register.contentType);
  res.end(client.register.metrics());
});

function registerRoute(route) {
  return (req, res) => {
    route.getModel().then((model) => {
      res.render(
        route.getView(),
        model
      );
    });
  };
}
router.get('/analytics', registerRoute(analyticsRoute));

module.exports = router;
