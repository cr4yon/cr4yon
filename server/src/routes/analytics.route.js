const metricService = require('../metrics/MetricService').instance();

const totalConnections = metrics => (
  metrics.map(m => m.value).reduce((v1, v2) => v1 + v2)
);

function getModel() {
  return Promise.resolve({
    title: '🖍 Cr4yon Analytics',
    metrics: {
      totalConnections: totalConnections(metricService.counters['socket-connection']),
      // counters: metricService.counters,
    },
  });
}

function getView() {
  return 'analytics';
}

module.exports = { getModel, getView };
