const formRoomState = room => ({
  name: room.name,
  members: Object.values(room.members).map(m => ({
    userName: m.userName,
    isConnected: m.isConnected,
    isDrawing: m.isDrawing,
    drawingColor: m.drawingColor,
  })),
});

const formDrawingState = room => ({
  points: room.messages ? room.messages.map(msg => msg.point) : [],
  drawingEvents: room.drawingEvents || [],
  room: formRoomState(room),
});

module.exports = {
  formRoomState,
  formDrawingState,
};
