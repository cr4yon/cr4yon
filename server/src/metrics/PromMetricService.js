const Counter = require('prom-client').Counter

class PromMetricService {
  constructor() {
    this.counters = {};
  }

  async incrementCounter(counterName, value) {
    if (!this.counters[counterName]) {
      this.counters[counterName] = new Counter({
        name: counterName,
        help: 'lala',
      });
    }

    this.counters[counterName].inc(value);
    console.log(`Counter[${counterName}] incremented by ${value}`);
  }
}

module.exports = PromMetricService;
