const metric = (value) => ({
  value,
  timestamp: Date.now(),
});

class FakeMetricService {
  constructor() {
    this.counters = {};
  }

  async incrementCounter(counterName, value) {
    if (!this.counters[counterName]) {
      this.counters[counterName] = [];
    }

    this.counters[counterName].push(
      metric(value)
    );
    console.log(`Counter[${counterName}] incremented by ${value}`);
  }
}

module.exports = FakeMetricService;
