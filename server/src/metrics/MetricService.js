const FakeMetricService = require('./FakeMetricService');
const PromMetricService = require('./PromMetricService');

const metricServiceType = {
  fake: 'fake',
  prom: 'prom',
};
const METRIC_SERVICE_TYPE = process.env.METRIC_SERVICE_TYPE || metricServiceType.prom;

function getMetricService(type) {
  switch (type) {
    case metricServiceType.prom:
      return new PromMetricService();
    case metricServiceType.fake:
    default:
      console.info('Using fake MetricService');
      return new FakeMetricService();
  }
}

const metricService = getMetricService(METRIC_SERVICE_TYPE);

module.exports = {
  instance: () => metricService,
};
