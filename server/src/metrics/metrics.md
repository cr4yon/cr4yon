# Crayon metrics
## Goal
Learn if the existing product is useful for the targeted customers

## Targeted customer
- Professionals who need to collaborate while not being physically together.
- Professionals who would use a real life whiteboard if they were together

## Assumptions and metrics
|Assumption | Analysis with metric |
|-----------|--------|
|Collaborative is proved useful if more than 1 customer are in the same room|number of rooms per number of people in them. x:number of people, y: number of rooms|
|Usage is well understood if customer joins room and draws|funnel: landing > form > draw|
|Text is discoverable|% first drawing that have text|
|Customer finds it useful if wants to share|% of click on share button with link + copy|
