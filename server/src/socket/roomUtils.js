module.exports = {
  findMemberForSocket: socketId => (
    room => (
      room && room.members && Object.values(room.members).find(m => m.socketId === socketId)
    )
  ),
};
