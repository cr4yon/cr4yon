const EventPublisher = require('./EventPublisher');

const publisher = new EventPublisher();

module.exports = {
  instance: () => publisher
}
