const sendDrawingStateForRoom = require('./sendDrawingStateForRoom');

const sendRoomStateOnNewMember = (io, roomStore) => (event) => {
  sendDrawingStateForRoom(io, roomStore, event.room.name);
}

sendRoomStateOnNewMember.isSubscriberFor = () => 'room-has-new-member';

module.exports = sendRoomStateOnNewMember;
