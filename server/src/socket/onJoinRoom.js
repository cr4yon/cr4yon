const RoomStore = require('../stores/RoomStore');
const metricService = require('../metrics/MetricService').instance();


module.exports = (socketId, acceptNewWhiteboardMember, declineWhiteboardJoining) => (msg) => {
  const roomStore = RoomStore.instance();
  const { userName, roomName } = msg;
  const buildMember = (userName) => ({
    userName,
    socketId,
    isConnected: true,
  });

  const onMemberAdded = (room) => {
    acceptNewWhiteboardMember(msg, room);
  }

  roomStore.getRoomByName(msg.roomName).then((roomResult) => {
    if (roomResult) {
      const member = roomResult.members[userName];
      if (member) {
        if (member.isConnected) {
          const errorMsg = `Member "${userName}" already connected to room "${roomName}"`;
          console.error(errorMsg);
          declineWhiteboardJoining(msg, errorMsg);
        } else {
          console.log('Member exists and is disconnected, updating', member);
          roomStore.updateMember(msg.roomName, buildMember(userName))
            .then(onMemberAdded)
            .catch((err) => declineWhiteboardJoining(msg, err));
        }
      } else {
        console.log(`Adding member to room[${msg.roomName}]`, member);
        roomStore.addMember(msg.roomName, buildMember(userName))
          .then(onMemberAdded)
          .catch((err) => declineWhiteboardJoining(msg, err));
      }

    } else {
      console.log('Room does not exist, creating room and adding member');
      const newRoom = {
        name: roomName,
        members: [ buildMember(userName) ],
      };
      return roomStore.insertNewRoom(roomName, newRoom).then((room) => {
        metricService.incrementCounter('rooms', 1);
        onMemberAdded(room);
      });
    }
  });
};
