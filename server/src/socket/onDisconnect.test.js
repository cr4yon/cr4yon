const onDisconnectCreator = require('./onDisconnect');
const EventPublisher = require('./EventPublisher');

test('does nothing if socket is part of no room', () => {
  const roomStore = {
    getRoomByName: jest.fn(),
    updateMember: jest.fn(),
  };
  const socket = {
    rooms: {},
    id: '123',
  };

  const notify = jest.fn();

  const onDisconnect = onDisconnectCreator(roomStore, socket, notify);
  onDisconnect();

  expect(roomStore.updateMember).not.toHaveBeenCalled();
  expect(notify).not.toHaveBeenCalled();
});

test('updates member in all rooms to be marked as disconnected', () => {
  const socket = {
    rooms: {
      theRoom: {},
      darkRoom: {},
      forgottenRoom: {},
    },
    id: '123',
  };
  const roomStore = {
    getRoomByName: jest.fn(),
    updateMember: jest.fn(),
  };
  const bob = {
    userName: 'bob',
    socketId: socket.id,
  }
  const theRoom = aRoom('theRoom', [ bob ]);
  const darkRoom = aRoom('darkRoom', [ bob ]);

  roomStore.getRoomByName = jest.fn(roomName => {
    switch(roomName) {
      case 'theRoom':
        return Promise.resolve(theRoom);
      case 'darkRoom':
        return Promise.resolve(darkRoom);
      case 'forgottenRoom':
        return Promise.resolve(aRoom('forgottenRoom'));
      default:
      return Promise.resolve();
    }
  });
  roomStore.updateMember.mockResolvedValue();

  const notify = jest.fn();

  const onDisconnect = onDisconnectCreator(roomStore, socket, notify);

  return onDisconnect().then(() => {
    expect(roomStore.updateMember).toHaveBeenCalledTimes(2);
    expect(notify).toHaveBeenCalledTimes(2);
  });
});

function aRoom(name, membersArray) {
  const members = {};
  membersArray && membersArray.forEach(m => members[m.userName] = m);

  return {
    name,
    members,
  };
}
