const { findMemberForSocket } = require('./roomUtils');

function copy(obj) {
  return JSON.parse(JSON.stringify(obj));
}

module.exports = (roomStore, socket, notifyRoomMembers) => () => {
  const roomNames = Object.keys(socket.rooms);

  const updateMemberIfRoomAndMemberExist = async (roomName) => {
    const room = await roomStore.getRoomByName(roomName);
    if (room) {
      const member = findMemberForSocket(socket.id)(room);
      if (member) {
        member.isConnected = false;
        await roomStore.updateMember(room.name, member);
        notifyRoomMembers(room.name);
      }
    }
  }

  return Promise.all(
    roomNames.map(updateMemberIfRoomAndMemberExist)
  )
};
