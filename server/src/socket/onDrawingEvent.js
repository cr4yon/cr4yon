const { findMemberForSocket } = require('./roomUtils');
const RoomStore = require('../stores/RoomStore');
const DomainEventPublisher = require('./DomainEventPublisher');

module.exports = (memberId, onDrawingEventRecorded) => (msg) => {
  const eventPublisher = DomainEventPublisher.instance();
  const roomStore = RoomStore.instance();

  return roomStore.getRoomByName(msg.roomName).then((room) => {
    const member = findMemberForSocket(memberId)(room);

    if (member) {
      const promises = [];

     return roomStore.saveDrawingEvent(room.name, msg).then(() => {
        onDrawingEventRecorded({ room, msg });
      })
        .then(() => {
          const messageHandler = getMessageHandlderForMessageType(msg.type)(eventPublisher, roomStore, room, member);
          return messageHandler(msg);
        })
    } else {
      console.error(`Member does not exist in room[${msg.roomName}]`, memberId);
      return Promise.reject({
        code: 'USER_NOT_CONNECTED',
        message: 'User is not a member of the room',
      });
    }
  });
};

const getMessageHandlderForMessageType = (type) => {
  switch (type) {
    case 'event.drawPoint':
      return drawPointMessageHandler;
    case 'event.stopDrawing':
      return stopDrawingMessageHandler;
    default:
      return () => (message) => console.warn(`No handlder for message type ${type}`);
  }
}

const drawPointMessageHandler = (eventPublisher, roomStore, room, member) => (message) => {
  if (!member.isDrawing) {
    member.isDrawing = true;
    member.drawingColor = message.point.color;
    return roomStore.updateMember(room.name, member).then(() => {
      eventPublisher.publish('room-member-updated', {
        room,
        member,
      });
    });
  } else {
    return Promise.resolve();
  }
};

const stopDrawingMessageHandler = (eventPublisher, roomStore, room, member) => (message) => {
  if (member.isDrawing) {
    member.isDrawing = false;
    return roomStore.updateMember(room.name, member).then(() => {
      eventPublisher.publish('room-member-updated', {
        room,
        member,
      });
    });
  } else {
    return Promise.resolve();
  }
};
