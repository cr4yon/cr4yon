const socketIo = require('socket.io');

const onDisconnectCreator = require('./onDisconnect');
const onDrawingEventCreator = require('./onDrawingEvent');
const onJoinRoomCreator = require('./onJoinRoom');
const DomainEventPublisher = require('./DomainEventPublisher');

const broadcastDrawingEvent = require('./broadcastDrawingEvent');
const sendRoomStateOnMemberUpdate = require('./sendRoomStateOnMemberUpdate');
const sendRoomStateOnNewMember = require('./sendRoomStateOnNewMember');

const { formRoomState, formDrawingState } = require('../presentation');
const sendRoomState = require('./sendRoomState');
const sendDrawingStateForRoom = require('./sendDrawingStateForRoom');

const roomStore = require('../stores/RoomStore').instance();
const metricService = require('../metrics/MetricService').instance();

const ioAwareEventSubscribers = [
  sendRoomStateOnMemberUpdate,
  sendRoomStateOnNewMember,
];

function setUpSocketHandlingForHttpServer(http) {
  const io = socketIo(http);
  const eventPublisher = DomainEventPublisher.instance();
  ioAwareEventSubscribers.forEach(subscriber => {
    eventPublisher.subscribe(
      subscriber.isSubscriberFor(),
      subscriber(io, roomStore)
    );
  });

  roomStore.onRoomDeleted((room) => {
    const socketsToDisconnect = Object.values(room.members)
    .map(m => io.sockets.connected[m.socketId])
    .filter(socket => !!socket);

    socketsToDisconnect.forEach((socket) => {
      socket.disconnect();
    });
  });

  io.on('connection', (socket) => {
    metricService.incrementCounter('socket_connection', 1);
    console.log('new connection!');

    socket.on(
      'disconnecting',
      onDisconnectCreator(roomStore, socket, (roomName) => {
        sendDrawingStateForRoom(io, roomStore, roomName);
      })
    );

    socket.on(
      'join-whiteboard',
      onJoinRoomCreator(
        socket.id,
        (message, theRoom) => {
          socket.join(theRoom.name, () => {
            socket.emit(
              'join-whiteboard',
              Object.assign(
                {},
                message,
                { result: 'ACCEPTED' },
              )
            );
            eventPublisher.publish('room-has-new-member', { room: theRoom });
          });
        },
        (message, reason) => {
          socket.emit(
            'join-whiteboard',
            Object.assign(
              {},
              message,
              { result: 'DECLINED', declineReason: reason },
            )
          );
        }
      )
    );

    socket.on(
      'drawing-event',
      onDrawingEventCreator(
        socket.id,
        (event) => {
          socket.broadcast.to(event.room.name).emit('drawing-event', event.msg);
        })
    );
  });

  return gracefulShutdown(io);
}

const disconnectMember = async (roomName, socket, member) => {
  try {
    // socket.disconnet();
    member.isConnected = false;
    return roomStore.updateMember(roomName, member);
  } catch (e) {
    console.error(e.message);
    console.warn(`Failed to disconnect socket[${socket.id}] for member`, member);
    return;
  }
}

const disconnectAllMembersFromRoom = async (roomName) => {
  try {
    const room = await roomStore.getExistingRoomByName(roomName);
    return Promise.all(
      Object.keys(room.members)
        .map((memberName) => room.members[memberName])
        .filter((member) => member.isConnected)
        .map((member) => {
          return disconnectMember(roomName, {id: 'fake-socket'}, member);
        })
    )
  } catch (e) {
    console.error(e.message);
    console.warn(`Room[${roomName}] does not exist, ignoring`);
  }
};

const gracefulShutdown = (io) => () => {
  const rooms = io.sockets.adapter.rooms;
  return Promise.all(
    Object.keys(rooms).map(disconnectAllMembersFromRoom)
  );
};

module.exports = {
  setUpSocketHandlingForHttpServer,
};
