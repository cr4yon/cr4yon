const { formRoomState } = require('../presentation');

function sendRoomState(io, room) {
  const roomState = formRoomState(room);
  io.to(room.name).emit('room-state', roomState);
}

module.exports = sendRoomState;
