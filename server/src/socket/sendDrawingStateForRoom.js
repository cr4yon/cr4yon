const { formDrawingState } = require('../presentation');

function sendDrawingStateForRoom(io, roomStore, roomName) {
  roomStore.getRoomByName(roomName)
  .then((room) => {
    roomStore.getEventsForRoomName(roomName).then((events) => {
      const drawingState = formDrawingState({ ...room, drawingEvents: events });
      io.to(roomName).emit('drawing-state', drawingState);
    })
  });
}
module.exports = sendDrawingStateForRoom;
