const EventPublisher = require('./EventPublisher');
const DomainEventPublisher = require('./DomainEventPublisher');
jest.mock('./DomainEventPublisher');

const onDrawingEventGenerator = require('./onDrawingEvent');
const RoomStore = require('../stores/RoomStore');
jest.mock('../stores/RoomStore');

test('throws if member is not in room', () => {
  const roomStore = mockedRoomStore();
  RoomStore.instance.mockReturnValue(roomStore);
  const theRoom = aRoom('theRoom');
  roomStore.getRoomByName.mockResolvedValue(theRoom);

  DomainEventPublisher.instance.mockReturnValue(new EventPublisher());
  
  const onDrawingEvent = onDrawingEventGenerator('123', jest.fn());
  
  const message = {
    roomName: 'theRoom',
    type: 'event.random',
  };

  return onDrawingEvent(message).catch((error) => {
    expect(error).not.toBeNull();
    expect(roomStore.getRoomByName).toHaveBeenCalledWith('theRoom');
  });
});

describe('if member is found', () => {
  test('saves event even if type unknown', () => {
    const roomStore = mockedRoomStore();
    RoomStore.instance.mockReturnValue(roomStore);
    const theRoom = aRoom('theRoom', [ aMember('bob', '123') ]);

    roomStore.getRoomByName.mockResolvedValue(theRoom);
    roomStore.saveDrawingEvent.mockResolvedValue();

    const eventPublisher = new EventPublisher();
    DomainEventPublisher.instance.mockReturnValue(eventPublisher);

    const onDrawingEventRecordedCallback = jest.fn();
    const onDrawingEvent = onDrawingEventGenerator('123', onDrawingEventRecordedCallback);

    const message = {
      roomName: 'theRoom',
      type: 'event.random',
    };

    return onDrawingEvent(message)
      .then(() => {
        expect(roomStore.saveDrawingEvent).toHaveBeenCalledWith('theRoom', message);
        expect(onDrawingEventRecordedCallback).toHaveBeenCalledWith({
          room: theRoom,
          msg: message,
        })
      });
  });

  test('saves event and updates member if draw point message and member not drawing', () => {
    const roomStore = mockedRoomStore();
    RoomStore.instance.mockReturnValue(roomStore);
    const bob = aMember('bob', '123');
    const theRoom = aRoom('theRoom', [ bob ]);

    roomStore.getRoomByName.mockResolvedValue(theRoom);
    roomStore.saveDrawingEvent.mockResolvedValue();
    roomStore.updateMember.mockResolvedValue();

    const eventPublisher = new EventPublisher();
    DomainEventPublisher.instance.mockReturnValue(eventPublisher);

    const memberUpdatedSubsriber = jest.fn();
    eventPublisher.subscribe('room-member-updated', memberUpdatedSubsriber);
    const onDrawingEventRecordedCallback = jest.fn();

    const onDrawingEvent = onDrawingEventGenerator('123', onDrawingEventRecordedCallback);

    const message = {
      roomName: 'theRoom',
      type: 'event.drawPoint',
      point: {
        color: '#123456',
      },
    };

    return onDrawingEvent(message)
      .then(() => {
        expect(onDrawingEventRecordedCallback).toHaveBeenCalledWith({
          room: theRoom,
          msg: message,
        });
        expect(memberUpdatedSubsriber).toHaveBeenCalledWith({
          room: theRoom,
          member: { ...bob, isDrawing: true },
        });
        expect(roomStore.updateMember).toHaveBeenCalledWith(
          'theRoom',
          {
            ...bob,
            isDrawing: true,
          }
        );
      });
  });

  test('saves event and does NOT update member if draw point message and member IS drawing', () => {
    const roomStore = mockedRoomStore();
    RoomStore.instance.mockReturnValue(roomStore);
    const bob = { ...aMember('bob', '123'), isDrawing: true };
    const theRoom = aRoom('theRoom', [ bob ]);

    roomStore.getRoomByName.mockResolvedValue(theRoom);
    roomStore.saveDrawingEvent.mockResolvedValue();
    roomStore.updateMember.mockResolvedValue();

    const eventPublisher = new EventPublisher();
    DomainEventPublisher.instance.mockReturnValue(eventPublisher);

    const memberUpdatedSubsriber = jest.fn();
    eventPublisher.subscribe('room-member-updated', memberUpdatedSubsriber);
    const onDrawingEventRecordedCallback = jest.fn();

    const onDrawingEvent = onDrawingEventGenerator('123', onDrawingEventRecordedCallback);

    const message = {
      roomName: 'theRoom',
      type: 'event.drawPoint',
      point: {
        color: '#123456',
      },
    };

    return onDrawingEvent(message)
      .then(() => {
        expect(onDrawingEventRecordedCallback).toHaveBeenCalledWith({
          room: theRoom,
          msg: message,
        });
        expect(memberUpdatedSubsriber).not.toHaveBeenCalled();
        expect(roomStore.updateMember).not.toHaveBeenCalled();
      });
  });

  test('saves event and updates member if stop drawing message and member IS drawing', () => {
    const roomStore = mockedRoomStore();
    RoomStore.instance.mockReturnValue(roomStore);
    const bob = { ...aMember('bob', '123'), isDrawing: true };
    const theRoom = aRoom('theRoom', [ bob ]);

    roomStore.getRoomByName.mockResolvedValue(theRoom);
    roomStore.saveDrawingEvent.mockResolvedValue();
    roomStore.updateMember.mockResolvedValue();

    const eventPublisher = new EventPublisher();
    DomainEventPublisher.instance.mockReturnValue(eventPublisher);

    const memberUpdatedSubsriber = jest.fn();
    eventPublisher.subscribe('room-member-updated', memberUpdatedSubsriber);
    const onDrawingEventRecordedCallback = jest.fn();

    const onDrawingEvent = onDrawingEventGenerator('123', onDrawingEventRecordedCallback);

    const message = {
      roomName: 'theRoom',
      type: 'event.stopDrawing',
      point: {
        color: '#123456',
      },
    };

    return onDrawingEvent(message)
      .then(() => {
        expect(onDrawingEventRecordedCallback).toHaveBeenCalledWith({
          room: theRoom,
          msg: message,
        });
        expect(memberUpdatedSubsriber).toHaveBeenCalledWith({
          room: theRoom,
          member: { ...bob, isDrawing: false },
        });
        expect(roomStore.updateMember).toHaveBeenCalledWith(
          'theRoom',
          {
            ...bob,
            isDrawing: false,
          }
        );
      });
  });

  test('saves event and does NOT update member if stop drawing message and member is NOT drawing', () => {
    const roomStore = mockedRoomStore();
    RoomStore.instance.mockReturnValue(roomStore);
    const bob = {
      socketId: '123',
      userName: 'bob',
      isDrawing: false,
    };
    const theRoom = aRoom('theRoom', [ bob ]);
    roomStore.getRoomByName.mockResolvedValue(theRoom);
    roomStore.saveDrawingEvent.mockResolvedValue();
    roomStore.updateMember.mockResolvedValue();

    const eventPublisher = new EventPublisher();
    DomainEventPublisher.instance.mockReturnValue(eventPublisher);

    const memberUpdatedSubsriber = jest.fn();
    eventPublisher.subscribe('room-member-updated', memberUpdatedSubsriber);
    const onDrawingEventRecordedCallback = jest.fn();

    const onDrawingEvent = onDrawingEventGenerator('123', onDrawingEventRecordedCallback);

    const message = {
      roomName: 'theRoom',
      type: 'event.stopDrawing',
      point: {
        color: '#123456',
      },
    };

    return onDrawingEvent(message)
      .then(() => {
        expect(onDrawingEventRecordedCallback).toHaveBeenCalledWith({
          room: theRoom,
          msg: message,
        });
        expect(memberUpdatedSubsriber).not.toHaveBeenCalled();
        expect(roomStore.updateMember).not.toHaveBeenCalled();
      });
    });
});

function mockedRoomStore() {
  return {
    getRoomByName: jest.fn(),
    saveDrawingEvent: jest.fn(),
    updateMember: jest.fn(),
  };
}

function aMember(userName, memberId) {
  return {
    userName,
    socketId: memberId,
    isDrawing: false,
  };
}

function aRoom(name, memberArray) {
  const members = {};
  memberArray && memberArray.forEach(m => members[m.userName] = m);
  return {
    name,
    members,
  };
}
