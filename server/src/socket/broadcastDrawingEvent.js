const broadcastDrawingEvent = (socket) => (event) => {
  socket.broadcast.to(event.room.name).emit('drawing-event', event.msg);
}

broadcastDrawingEvent.isSubscriberFor = () => 'drawing-event-recorded';

module.exports = broadcastDrawingEvent;
