const sendRoomState = require('./sendRoomState');

const sendRoomStateOnMemberUpdate = (io) => (event) => {
  sendRoomState(io, event.room);
}

sendRoomStateOnMemberUpdate.isSubscriberFor = () => 'room-member-updated';

module.exports = sendRoomStateOnMemberUpdate;
