const EventPublisher = require('./EventPublisher');

test('calls every subsriber when an event is published', () => {
  const publisher = new EventPublisher();

  const subsriberForMusicEvent = jest.fn();
  publisher.subscribe('music', subsriberForMusicEvent);

  const subsriberForSportEvent = jest.fn();
  publisher.subscribe('sport', subsriberForSportEvent);

  const musicEvent = { type: 'newConcert' };
  publisher.publish('music', musicEvent);

  expect(subsriberForMusicEvent).toHaveBeenCalledWith(musicEvent);
  expect(subsriberForSportEvent).not.toHaveBeenCalled();
});

test('does nothing if an event has no subscriber', () => {
  const publisher = new EventPublisher();

  const subsriberForMusicEvent = jest.fn();
  publisher.subscribe('music', subsriberForMusicEvent);

  const subsriberForSportEvent = jest.fn();
  publisher.subscribe('sport', subsriberForSportEvent);

  const musicEvent = { type: 'newConcert' };
  publisher.publish('lalala', musicEvent);

  expect(subsriberForMusicEvent).not.toHaveBeenCalled();
  expect(subsriberForSportEvent).not.toHaveBeenCalled();
});
