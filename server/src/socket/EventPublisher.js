class EventPublisher {
  constructor() {
    this.subscribers = {};
  }

  subscribe(eventName, callback) {
    if (!this.subscribers[eventName]) {
      this.subscribers[eventName] = [];
    }
    this.subscribers[eventName].push(callback);
  }

  publish(eventName, event) {
    this.subscribers[eventName] && this.subscribers[eventName].forEach(s => s(event));
  }
}

module.exports = EventPublisher;
