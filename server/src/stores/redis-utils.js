module.exports = (redisClient) => ({
  addTtlIfAbsent: function(key, ttl) {
    return new Promise((resolve, reject) => {
      redisClient.ttl(key, (res) => {
        if (!res) {
          redisClient.expire(key, ttl, (res) => {
            resolve();
          })
        } else {
          resolve();
        }
      });
    })
  }
})
