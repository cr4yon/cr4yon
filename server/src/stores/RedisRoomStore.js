const redis = require('redis');
const RedisEventStore = require('./RedisEventStore');
const RedisRoomMemberStore = require('./RedisRoomMemberStore');

const REDIS_PORT = process.env.REDIS_PORT || 6379;
const REDIS_HOST = process.env.REDIS_HOST || 'localhost';

function isFlushDbOnStart() {
  const envValue = process.env.FLUSH_DB_ON_START;

  if (typeof envValue === 'string') {
    return envValue === 'true';
  } else if (typeof envValue === 'boolean') {
    return envValue;
  } else {
    return false;
  }
}
const FLUSH_DB_ON_START = isFlushDbOnStart();

function checkRoomNameIsValid(roomName) {
  return roomName instanceof String && roomName.length > 0;
}

function copy(obj) {
  return JSON.parse(JSON.stringify(obj));
}

function rejectWithError(error) {
  return new Promise((resolve, reject) => reject(error));
}

class RedisRoomStore {
  constructor(roomExpirationTimeInSeconds) {
    this.redisClient = redis.createClient(REDIS_PORT, REDIS_HOST);
    if (FLUSH_DB_ON_START) this.flushDb();
    this.eventStore = new RedisEventStore(roomExpirationTimeInSeconds, this.redisClient);
    this.roomMemberStore = new RedisRoomMemberStore(roomExpirationTimeInSeconds, this.redisClient);
    this.roomExpirationTimeInSeconds = roomExpirationTimeInSeconds;
    this.onRoomDeletedCallback = null;
  }

  flushDb() {
    console.info('flushing DB...');
    return new Promise((resolve, reject) => {
      this.redisClient.flushdb((err, res) => {
        if (!err) {
          resolve();
        } else {
          console.error('Could not flush db', err);
          reject(err);
        }
      });
    });
  }

  saveDrawingEvent(roomName, event) {
    return this.eventStore.saveEventForRoomName(roomName, event);
  }

  getEventsForRoomName(roomName) {
    return this.eventStore.getStringifiedEventsForRoomName(roomName)
      .then((events) => events ? events.map(e => JSON.parse(e)) : []);
  }

  onRoomDeleted(callback) {
    this.onRoomDeletedCallback = callback;
  }

  deleteRoom(room) {
    console.info('Deleting expired room', room.name);
    delete this.rooms[room.name];
    if (this.onRoomDeletedCallback) {
      this.onRoomDeletedCallback(room);
    }
  }

  getRoomByName(roomName) {
    const collect = async () => {
      const room = await this.getRoomFromRedis(roomName);

      if (!room) return null;

      const membersAndEvents = await Promise.all([
        this.roomMemberStore.getMembers(roomName),
        this.getEventsForRoomName(roomName),
      ]);

      return {
        ...room,
        members: membersAndEvents[0],
        drawingEvents: membersAndEvents[1],
        expiryDate: 'random', // TODO
      }
    };

    return collect();
  }

  getRoomFromRedis(roomName) {
    return new Promise((resolve, reject) => {
      this.redisClient.get(`room:${roomName}`, (error, roomAsString) => {
        if (error) reject(error);
        const room = roomAsString ? JSON.parse(roomAsString) : null;
        resolve(room);
      });
    });
  }

  getExistingRoomByName(roomName) {
    return this.getRoomByName(roomName).then((roomFetched) => {
      if (!roomFetched) {
        return rejectWithError(new Error('Room not found'));
      }
      return roomFetched;
    });
  }

  addMember(roomName, member) {
    const execute = async () => {
      const roomRes = await this.getExistingRoomByName(roomName);
      await this.roomMemberStore.addMember(roomName, member);
      return this.getExistingRoomByName(roomName);
    }
    return execute();
  }

  updateMember(roomName, member) {
    const execute = async () => {
      const roomRes = await this.getExistingRoomByName(roomName);
      await this.roomMemberStore.updateMember(roomName, member);
      return this.getExistingRoomByName(roomName);
    }
    return execute();
  }

  insertNewRoom(roomName, { name, expiryDate, members }) {
    checkRoomNameIsValid(roomName);

    const collect = async () => {
      const roomFetched = await this.getRoomByName(roomName);
      if (roomFetched) {
        throw new Error(`room ${roomName} already exists`);
      }

      await Promise.all([
        this.setRoom(roomName, { name, expiryDate }),
        Promise.all(
          members.map((m) => this.roomMemberStore.addMember(roomName, m))
        )
      ]);

      const roomAndMembers = await Promise.all([
        this.getRoomByName(roomName),
        this.roomMemberStore.getMembers(roomName),
      ]);

      return {
        ...roomAndMembers[0],
        members: roomAndMembers[1],
        drawingEvents: [],
      };
    };

    return collect().then(res => {
      return res;
    });
  }

  setRoom(roomName, room) {
    return new Promise((resolve, reject) => {
      this.redisClient.set(
        `room:${roomName}`,
        JSON.stringify(room),
        'EX',
        this.roomExpirationTimeInSeconds,
        (error, result) => {
          if (error) {
            console.error('Error setting room', error);
            reject(error);
          } else {
            resolve(room);
          }
        }
      );
    });
  }
}


module.exports = RedisRoomStore;
