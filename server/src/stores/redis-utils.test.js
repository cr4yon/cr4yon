const redisUtils = require('./redis-utils');


describe('addTtlIfAbsent', () => {
  it('does nothing if ttl exists', () => {
    const redisClient = {
      ttl: jest.fn((key, cb) => cb(10)),
      expire: jest.fn(),
    };

    const theKey = 'theKey';

    return redisUtils(redisClient).addTtlIfAbsent(theKey, 10)
      .then(() => {
        expect(redisClient.ttl).toHaveBeenCalled();
        expect(redisClient.ttl.mock.calls[0][0]).toBe(theKey);
        expect(redisClient.expire).not.toHaveBeenCalled();
      })
  });

  it('sets ttl if does not exist', () => {
    const redisClient = {
      ttl: jest.fn((key, cb) => cb()),
      expire: jest.fn((key, ttl, cb) => cb()),
    };

    const theKey = 'theKey';

    return redisUtils(redisClient).addTtlIfAbsent(theKey, 10)
      .then(() => {
        expect(redisClient.ttl).toHaveBeenCalled();
        expect(redisClient.ttl.mock.calls[0][0]).toBe(theKey);
        expect(redisClient.expire).toHaveBeenCalled();
        expect(redisClient.expire.mock.calls[0][0]).toBe(theKey);
        expect(redisClient.expire.mock.calls[0][1]).toBe(10);
      })
  });
});
