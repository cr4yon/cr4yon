const redisUtils = require('./redis-utils');

const redisKey = (roomName) => `room:${roomName}.drawingEvents`;

class RedisDrawingEventStore {
  constructor(roomExpirationTimeInSeconds, redisClient) {
    this.redisClient = redisClient;
    this.roomExpirationTimeInSeconds = roomExpirationTimeInSeconds;
  }

  getStringifiedEventsForRoomName(roomName) {
    return new Promise((resolve, reject) => {
      this.redisClient.lrange(redisKey(roomName), 0, -1, (error, result) => {
        if (error) reject(error);
        resolve(result ? result : []);
      });
    });
  }

  saveEventForRoomName(roomName, event) {
    return this.addEventOnlyIfListExists(roomName, event)
      .catch((err) => {
        console.warn(`Could not add event to existing list for room[${roomName}]`, err);
        return this.addEventWithNewListAndExpiry(roomName, event);
      })
  }

  addEventOnlyIfListExists(roomName, event) {
    return this.addEventWithFunction(roomName, 'rpushx', event);
  }

  addEventWithNewListAndExpiry(roomName, event) {
    return this.addEventWithFunction(roomName, 'rpush', event)
      .then((result) => {
        return redisUtils(this.redisClient).addTtlIfAbsent(redisKey(roomName), this.roomExpirationTimeInSeconds)
          .then(() => result);
      });
  }

  addEventWithFunction(roomName, fn, event) {
    return new Promise((resolve, reject) => {
      this.redisClient[fn](
        redisKey(roomName),
        JSON.stringify(event),
        (error, result) => {
          if (error) {
            reject(error);
          } else {
            if (result === 0) {
              reject('Event was not saved');
            } else {
              resolve();
            }
          }
        }
      );
    });
  }
}


module.exports = RedisDrawingEventStore;
