const redisUtils = require('./redis-utils');

const mappOnObj = (obj, mapFn) => {
  const newObj = {};
  Object.keys(obj).forEach((k) => {
    newObj[k] = mapFn(obj[k]);
  });
  return newObj;
}

class RedisDrawingEventStore {
  constructor(roomExpirationTimeInSeconds, redisClient) {
    this.redisClient = redisClient;
    this.roomExpirationTimeInSeconds = roomExpirationTimeInSeconds;
  }

  getMembers(roomName) {
    return new Promise((resolve, reject) => {
      this.redisClient.hgetall(
        `room:${roomName}.members`,
        (error, result) => {
          if (error) {
            reject(error);
          } else {
            resolve(result ? mappOnObj(result, (v) => JSON.parse(v)) : {});
          }
        }
      );
    });
  }

  getMemberByName(roomName, memberName) {
    return new Promise((resolve, reject) => {
      this.redisClient.hget(
        `room:${roomName}.members`,
        memberName,
        (error, result) => {
          if (error) {
            reject(error);
          } else {
            resolve(JSON.parse(result));
          }
        }
      );
    });
  }

  addMember(roomName, member) {
    return (async () => {
      const existingMember = await this.getMemberByName(roomName, member.userName);
      if (existingMember) throw new Error('Member already exists');

      return this.setMember(roomName, member);
    })();
  }

  updateMember(roomName, member) {
    return (async () => {
      const existingMember = await this.getMemberByName(roomName, member.userName);
      if (!existingMember) throw new Error('Member does not exist');

      return this.setMember(roomName, member);
    })();
  }

  setMember(roomName, member) {
    return new Promise((resolve, reject) => {
      this.redisClient.hset(
        `room:${roomName}.members`,
        member.userName,
        JSON.stringify(member),
        (error, result) => {
          if (error) {
            console.error(error);
            reject(error);
          } else {
            resolve(result);
          }
        }
      );
    })
      .then((result) => {
        return redisUtils(this.redisClient)
          .addTtlIfAbsent(`room:${roomName}.members`, this.roomExpirationTimeInSeconds)
          .then(() => result);
      })
  }
}


module.exports = RedisDrawingEventStore;
