const InMemRoomStore = require('./InMemoryRoomStore');
const RedisRoomStore = require('./RedisRoomStore');

const TWENTY_FOUR_HOURS_IN_SECONDS = 24 * 3600;
const ROOM_TTL_IN_SECONDS = process.env.ROOM_TIME_TO_LIVE_IN_SECONDS ||
    TWENTY_FOUR_HOURS_IN_SECONDS;

const THREE_SECONDS = 3;
const ROOM_EXPIRY_CHECK_FREQUENCY = process.env.ROOM_EXPIRY_CHECK_FREQUENCY ||
    THREE_SECONDS;

const roomStoreTypes = {
  redis: 'redis',
  inMemory: 'inMemory',
};
const ROOM_STORE_TYPE = process.env.ROOM_STORE_TYPE || roomStoreTypes.inMemory;

function getRoomStore(type) {
  switch (type) {
    case roomStoreTypes.redis:
      console.info('Using Redis for room data storage');
      return new RedisRoomStore(ROOM_TTL_IN_SECONDS, ROOM_EXPIRY_CHECK_FREQUENCY);
    case roomStoreTypes.inMemory:
    default:
      console.info('Using InMemory for room data storage');
      return new InMemRoomStore(ROOM_TTL_IN_SECONDS, ROOM_EXPIRY_CHECK_FREQUENCY);
  }
}

const roomStore = getRoomStore(ROOM_STORE_TYPE);

module.exports = {
  instance: () => roomStore,
};
