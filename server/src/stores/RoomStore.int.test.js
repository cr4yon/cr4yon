const InMemoryRoomStore = require('./InMemoryRoomStore');
const RedisRoomStore = require('./RedisRoomStore');

const ROOM_STORE_TYPE = 'redis';
function getRoomStore(type, ttl) {
  switch(type) {
    case 'redis':
      return new RedisRoomStore(ttl);
    default:
      return new InMemoryRoomStore(ttl, 10);
  }
}

[ 'inMemory', 'redis' ].forEach((storeType) => {
  const roomStore = getRoomStore(storeType, 100);

  afterEach(() => {
    return roomStore.flushDb();
  })

  describe(`RoomStore[${storeType}]`, () => {
    describe('insertNewRoom', () => {
      test('creates new room if does not exist', () => {
        return (async () => {
          await roomStore.insertNewRoom('theRoom', aRoom('theRoom', []));
          const room = await roomStore.getRoomByName('theRoom');

          expect(room.name).toEqual('theRoom');
          expect(room.drawingEvents).toHaveLength(0);
          expect(room.expiryDate).toBeDefined();
          expect(Object.keys(room.members)).toHaveLength(0);
        })();
      });

      test('throws if room already exists', () => {
        return (async () => {
          let error;
          try {
            await roomStore.insertNewRoom('theRoom', aRoom('theRoom', []));
            const room = await roomStore.getRoomByName('theRoom');
            expect(room.name).toEqual('theRoom');
            await roomStore.insertNewRoom('theRoom', aRoom('theRoom', []));
          } catch (e) {
            error = e;
          }

          expect(error).toBeDefined();
        })()
      });
    });

    describe('saveDrawingEvent', () => {
      test('pushes event to array', () => {

        return (async () => {
          await roomStore.insertNewRoom('theRoom', aRoom('theRoom', []));
          await roomStore.addMember('theRoom', { userName: 'bob' });

          const event1 = { type: 'random', data: 'first' };
          await roomStore.saveDrawingEvent('theRoom', event1);
          const event2 = { type: 'random', data: 'second' };
          await roomStore.saveDrawingEvent('theRoom', event2);

          const room = await roomStore.getRoomByName('theRoom');
          expect(room.drawingEvents).toEqual([ event1, event2 ]);
          // expect(room.drawingEvents[1]).toEqual(event2);
        })()
      });
    });

    describe('getEventsForRoomName', () => {
      test('returns list of events', () => {
        return (async () => {
          await roomStore.insertNewRoom('theRoom', aRoom('theRoom', []));
          await roomStore.addMember('theRoom', { userName: 'bob' });

          const event1 = { type: 'random', data: 'foo' };
          await roomStore.saveDrawingEvent('theRoom', event1);
          const event2 = { type: 'random', data: 'bar' };
          await roomStore.saveDrawingEvent('theRoom', event2);

          const events = await roomStore.getEventsForRoomName('theRoom');
          expect(events[0]).toEqual(event1);
          expect(events[1]).toEqual(event2);
        })()
      });
    });

    describe('addMember', () => {
      test('adds member if does not exist', () => {
        return (async () => {
          await roomStore.insertNewRoom('theRoom', aRoom('theRoom', []));
          const bob = {
            userName: 'bob',
            socketId: '123',
          };
          const room = await roomStore.addMember('theRoom', bob);

          expect(room.members['bob']).toEqual(bob);
        })()
      });

      test('throws if member already exists', () => {
        return (async () => {
          await roomStore.insertNewRoom('theRoom', aRoom('theRoom', []));
          const bob = {
            userName: 'bob',
            socketId: '123',
          };
          await roomStore.addMember('theRoom', bob);
          const room = await roomStore.getRoomByName('theRoom');
          expect(room.members['bob']).toEqual(bob);

          let error;
          try {
            await roomStore.addMember('theRoom', bob);
          } catch (e) {
            error = e;
          }
          expect(error).toBeDefined();
        })()
      });
    });

    describe('updateMember', () => {
      test('throws if member does not exist', () => {
        return (async () => {
          await roomStore.insertNewRoom('theRoom', aRoom('theRoom', []));
          const bob = {
            userName: 'bob',
            socketId: '123',
          };
          let error;
          try {
            await roomStore.updateMember('theRoom', bob);
          } catch (e) {
            error = e;
          }
          expect(error).toBeDefined();
        })()
      });

      test('updates member if exists', () => {
        return (async () => {
          await roomStore.insertNewRoom('theRoom', aRoom('theRoom', []));
          const bob = {
            userName: 'bob',
            socketId: '123',
          };
          await roomStore.addMember('theRoom', bob);

          let error;
          const room = await roomStore.updateMember('theRoom', { ...bob, socketId: '333' });

          expect(room.members['bob'].socketId).toEqual('333');
        })()
      });
    });
  });
});


function aRoom(name, members) {
  return {
    name,
    members
  }
}
