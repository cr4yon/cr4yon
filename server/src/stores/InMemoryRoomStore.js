const arrayToObj = (array, propertyToUseAsKey) => {
  const obj = {};
  array.forEach(entry => obj[entry[propertyToUseAsKey]] = entry)
  return obj;
}

function checkRoomNameIsValid(roomName) {
  return roomName instanceof String && roomName.length > 0;
}

class RoomStore {
  constructor(roomExpirationTimeInSeconds, checkExpiredRoomsEvery) {
    this.rooms = {};
    this.roomExpirationTime = roomExpirationTimeInSeconds;
    this.onRoomDeletedCallback = null;

    setInterval(
      () => this.deleteExpiredRooms(),
      checkExpiredRoomsEvery * 1000
    );
  }

  flushDb() {
    return new Promise((resolve, reject) => {
      this.rooms = {};
      resolve();
    });
  }

  onRoomDeleted(callback) {
    this.onRoomDeletedCallback = callback;
  }

  deleteExpiredRooms() {
    Object.keys(this.rooms).forEach((roomName) => {
      const room = this.getRoomByName(roomName);

      if (room.expiryDate < Date.now()) {
        this.deleteRoom(room);
      }
    });
  }

  deleteRoom(room) {
    console.info('Deleting expired room', room.name);
    delete this.rooms[room.name];
    if (this.onRoomDeletedCallback) {
      this.onRoomDeletedCallback(room);
    }
  }

  getRoomByName(roomName) {
    return new Promise((resolve, reject) => {
      resolve(roomName && this.rooms[roomName]);
    });
  }

  insertNewRoom(roomName, { name, members }) {
    checkRoomNameIsValid(roomName);
    return this.getRoomByName(roomName).then((roomFetched) => {
      if (!roomFetched) {
        return this.setRoom(
          roomName,
          {
            name,
            members: arrayToObj(members, 'userName'),
            drawingEvents: [],
            expiryDate: Date.now() + (this.roomExpirationTime * 1000),
          }
        );
      }
      return rejectWithError(`room ${roomName} already exists`);
    });
  }

  setRoom(roomName, room) {
    return new Promise((resolve, reject) => {
      this.rooms[roomName] = {
        ...room,
        expiryDate: Date.now() + (this.roomExpirationTime * 1000),
      };
      resolve(this.rooms[roomName]);
    });
  }

  getExistingRoomSync(roomName) {
    const room = this.rooms[roomName];
    if (!room) throw new Error(`room[${roomName} not found`);
    return room;
  }

  saveDrawingEvent(roomName, event) {
    return new Promise((resolve) => {
      const room = this.getExistingRoomSync(roomName);
      room.drawingEvents.push(event);
      resolve();
    });
  }

  getEventsForRoomName(roomName) {
    return new Promise((resolve) => {
      const room = this.getExistingRoomSync(roomName);
      resolve(room.drawingEvents);
    });
  }

  getExistingRoomByName(roomName) {
    return this.getRoomByName(roomName).then((roomFetched) => {
      if (!roomFetched) {
        return rejectWithError(new Error(`room[${roomName} not found`));
      }
      return roomFetched;
    });
  }

  addMember(roomName, member) {
    return new Promise((resolve, reject) => {
      const room = this.getExistingRoomSync(roomName);
      const existingMember = room.members[member.userName];

      if (existingMember) {
        reject('Member already exists');
      } else {
        room.members[member.userName] = member;
        resolve(room);
      }
    });
  }

  updateMember(roomName, member) {
    return new Promise((resolve) => {
      const room = this.getExistingRoomSync(roomName);
      const existingMember = room.members[member.userName];

      if (!existingMember) {
        reject('Member does not exist');
      } else {
        room.members[member.userName] = member;
        resolve(room);
      }
    });
  }
}


module.exports = RoomStore;
