module.exports = {
    "extends": "airbnb-base",
    "rules": {
        "arrow-parens": 2,
        "indent": [2, 2],
        "comma-dangle": [
            "error",
            {
                "arrays": "always-multiline",
                "objects": "always-multiline",
                "imports": "always-multiline",
                "exports": "always-multiline",
                "functions": "ignore",
            }
        ]
    },
};
